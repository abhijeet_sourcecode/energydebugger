package plugin;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import nus.edu.sg.Global;
import nus.edu.sg.Log;
import nus.edu.sg.ds.ClassNode;
import nus.edu.sg.ds.Defect;
import nus.edu.sg.ds.EnergyBug;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import plugin.utility.Parser;
import plugin.views.DebugResults;
import plugin.views.ProjectView;

/**
 * The Class PerspectiveFactory. This class is responsible for displaying the
 * initial layout
 */
public class PerspectiveFactory implements IPerspectiveFactory {

	final double WIDTH_OF_WIDEST_IMAGE = 350;// px;

	public static String editorArea;

	final static String cn = "PerspectiveFactory";

	public static IFolderLayout middle;

	/**
	 * Initialization Code Initiates the Intra and Inter Event Handler Lists
	 * Initiates the message console handler, stored in Global.out Gathers the
	 * list of projects in workspace, stored in ProjectView.displayList
	 */
	public static void initialization() {

		Log.d("[" + cn + "] initialization");

		Log.i("Initializing message console");
		final MessageConsole myConsole = findConsole(Global.consoleName);
		Global.out = myConsole.newMessageStream();
		Global.pathToProjects = new HashMap<String, String>();
		Global.sourceToLineMap = new HashMap<String, ClassNode>();
		Global.debugList = new Defect[1];
		final EnergyBug b = new EnergyBug("NOT", "INIT");
		b.method = "data";
		b.file = "Sample";
		b.caller = "Sample";
		Global.debugList[0] = b;

		try {
			Log.i("[" + cn + "] Gathering list of projects in the workspace");
			Parser.getListOfProjects();

		} catch (final CoreException e) {
			e.printStackTrace();
			Log.e("CoreException happened while in the parsing code" + e);
		} catch (final Exception e) {
			Log.e("Some (non-core) exception happend while in the parsing code " + e);
			e.printStackTrace();
		}
		Global.initPresent = true;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui
	 * .IPageLayout)
	 */
	@Override
	public void createInitialLayout(IPageLayout layout) {

		editorArea = layout.getEditorArea();

		final IFolderLayout left = layout.createFolder("left", IPageLayout.LEFT, (float) 0.20, editorArea);
		left.addView(ProjectView.ID);

		final IFolderLayout leftBottom = layout.createFolder("leftBottom", IPageLayout.BOTTOM, (float) 0.50, "left");
		leftBottom.addView(DebugResults.ID);

		final IFolderLayout bottom = layout.createFolder("under", IPageLayout.BOTTOM, (float) 0.70, editorArea);
		bottom.addView(IConsoleConstants.ID_CONSOLE_VIEW);

		layout.addView("plugin.views.imageview", IPageLayout.RIGHT, (float) 0.8, editorArea);

		int toRemove = 5;
		final String fs = System.getProperty("file.separator");
		if (!fs.contains("/")) {
			toRemove = 6;
		}

		/* Set path to config files needed by the AI analysis */
		final Bundle bundle = FrameworkUtil.getBundle(PerspectiveFactory.class);
		try {
			URL url;
			url = FileLocator.find(bundle, new Path(""), null);
			if (url != null) {

				// Need to remove the "file:" prefix as well
				Global.plugindirectory = FileLocator.toFileURL(url).toString().substring(toRemove);
				Log.d("[" + cn + "] Plugin directory " + Global.plugindirectory);
			}
			url = FileLocator.find(bundle, new Path("input/guideline.xml"), null);
			if (url != null) {

				Global.xmlFilePath = FileLocator.toFileURL(url).toString().substring(toRemove);
				System.out.println(Global.xmlFilePath);
			}

			url = FileLocator.find(bundle, new Path("input/guidelineHotspot.xml"), null);
			if (url != null) {

				Global.hotspotXmlFilePath = FileLocator.toFileURL(url).toString().substring(toRemove);
				System.out.println(Global.hotspotXmlFilePath);
			}

		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Log.d("[" + cn + "] Perspective creation complete");

	}

	/**
	 * Find console. Find or creates a new console to display plugin output
	 * 
	 * @param name
	 *            name of the console
	 * @return the message console
	 */
	private static MessageConsole findConsole(String name) {
		final ConsolePlugin plugin = ConsolePlugin.getDefault();
		final IConsoleManager conMan = plugin.getConsoleManager();
		final IConsole[] existing = conMan.getConsoles();
		for (final IConsole element : existing) {
			if (name.equals(element.getName())) {
				System.out.println("Message console " + name + " found, returning");
				return (MessageConsole) element;
			}
		}

		System.out.println("Message console " + name + " not found, creating a new one");
		final MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}

}