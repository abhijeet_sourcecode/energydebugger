package plugin.views;

import java.io.File;

import nus.edu.sg.DebuggerMain;
import nus.edu.sg.Global;
import nus.edu.sg.Log;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import plugin.PerspectiveFactory;
import plugin.utility.Parser;

public class ProjectView extends ViewPart {
	public static final String ID = "plugin.views.projectview";

	/** The viewer. */
	public static TreeViewer viewer;

	/** The refresh action. */
	private Action action1;

	/** The double click action. */
	private Action doubleClickAction;

	private final String cn = "ProjectView";

	private Composite localParent;

	/**
	 * The constructor.
	 */
	public ProjectView() {
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 * 
	 * @param parent
	 *            the parent
	 */
	@Override
	public void createPartControl(Composite parent) {

		localParent = parent;
		System.err.println("project view init");
		Log.d("[" + cn + "] Creating ProjectView");

		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		// Log.i("root" + root.getLocation().toOSString());
		Global.rootLocation = root.getLocation().toOSString();

		Log.d("Root location " + Global.rootLocation);

		if (!Global.initPresent) {
			PerspectiveFactory.initialization();
		}

		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new FileTreeContentProvider());
		viewer.setLabelProvider(new FileTreeLabelProvider());
		viewer.setInput(new File(Global.rootLocation));

		PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(), "EnergyDebugger.viewer");

		makeActions();
		hookDoubleClickAction();
		contributeToActionBars();

	}

	/**
	 * @see IViewPart.init(IViewSite)
	 */
	@Override
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
	}

	/**
	 * Contribute to action bars.
	 */
	private void contributeToActionBars() {
		final IActionBars bars = getViewSite().getActionBars();
		// fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * Fill local tool bar.
	 * 
	 * @param manager
	 *            the manager
	 */
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(action1);
		// manager.add(action2);
	}

	/**
	 * Make actions.
	 */
	private void makeActions() {
		action1 = new Action() {
			@Override
			public void run() {
				showMessage("This part is still buggy, restart the perspective for correct results");
				PerspectiveFactory.initialization();
				viewer.refresh();
				System.out.println("invoked refresh");
			}
		};
		action1.setText("Refresh");
		action1.setToolTipText("Refresh");
		action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_ELCL_SYNCED));

		doubleClickAction = new Action() {
			@Override
			public void run() {
				final ISelection selection = viewer.getSelection();

				Global.selectedProject = getSelectedProject(((IStructuredSelection) selection).getFirstElement()
						.toString());

				if (Global.selectedProject == null) {
					showMessage("Sorry, can't find the selected project!");
					return;
				}

				Log.i("[" + cn + "] User selected project " + Global.selectedProject);
				System.out.println("Path to project " + Global.pathToSelectedProject);

				try {
					Parser.processProject();

					final String s = startFileSelector(localParent);
					if (s != null) {
						Global.logPath = s;
						Log.i("[" + cn + "] User selected file " + Global.logPath);
						Log.i("[" + cn + "] Started Analysis");
						final String[] args = {};
						DebuggerMain.main(args);
					}

				} catch (final CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			private String startFileSelector(Composite localParent) {
				final FileDialog dialog = new FileDialog(localParent.getShell(), SWT.OPEN);
				dialog.setText("Open");
				final String path = dialog.open();
				if (path == null) {
					Log.i("No file selected");
					return null;
				}
				Log.i("[" + cn + "]selected " + path);
				return path;

			}

			private String getSelectedProject(String string) {
				// Log.d("[" + cn + "] User selected " + string);

				Global.pathToSelectedProject = string;

				for (final String p : Global.pathToProjects.keySet()) {
					final String path = Global.pathToProjects.get(p);
					if (string.replace("\\", "/").startsWith(path.replace("\\", "/"))) {
						return p;
					}
				}

				return null;
			}

		};
	}

	/**
	 * Hook double click action.
	 */
	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	/**
	 * Show message.
	 * 
	 * @param message
	 *            the message
	 */
	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(), "EnergyDebugger Info", message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@Override
	public void dispose() {
		Global.initPresent = false;
	}
}
