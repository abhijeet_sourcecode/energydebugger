package plugin.views;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import nus.edu.sg.Global;
import nus.edu.sg.Log;
import nus.edu.sg.ds.ClassNode;
import nus.edu.sg.ds.Defect;
import nus.edu.sg.ds.LoopNode;
import nus.edu.sg.ds.MethodNode;
import nus.edu.sg.ds.Tupple;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.ViewPart;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.sun.corba.se.impl.orbutil.graph.Graph;

public class DebugResults extends ViewPart {
	public static final String ID = "plugin.views.debugresults";
	private Action doubleClickAction;
	private static TableViewer viewer;
	protected String cn = "DebugResults";

	public static Image bugImage = getImage("nibbler_b.png");
	public static Image hotspotImage = getImage("nibbler_h.png");

	@Override
	public void createPartControl(Composite parent) {

		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());

		makeActions();
		hookDoubleClickAction();

	}

	public static Image getImage(String s) {

		final Bundle bundle = FrameworkUtil.getBundle(DebugResults.class);
		final URL url = FileLocator.find(bundle, new Path("/icons/" + s), null);

		final ImageDescriptor image = ImageDescriptor.createFromURL(url);

		return image.createImage();

	};

	private void makeActions() {
		doubleClickAction = new Action() {
			private JFrame jFrame;
			private JLabel headerLabel;
			private JLabel statusLabel;
			private JLabel msglabel;
			private JPanel controlPanel;
			private Graph graph;

			@Override
			public void run() {

				// showGraph();
				// System.out.println("double click registered");
				// if (true) {
				// return;
				// }

				final ISelection selection = viewer.getSelection();

				Global.selectedDebug = ((IStructuredSelection) selection).getFirstElement();

				// final String className = ((Defect)
				// Global.selectedDebug).getFile();
				final Defect defect = (Defect) Global.selectedDebug;
				final String callerName = defect.getCaller();
				if (defect.after != null && defect.before != null) {
					ImageView.populate(defect);
				}

				if (callerName.equals("Sample")) {
					showMessage("This is a sample.");
					return;
				}

				final StringTokenizer st = new StringTokenizer(callerName, "->");
				String className = null;
				String methodName = null;
				int count = 0;
				while (st.hasMoreElements()) {
					final String temp = (String) st.nextElement();
					if (count == 0) {
						className = temp;
						if (className.contains(";")) {
							className = className.substring(0, className.indexOf(";"));
							final int idx = className.indexOf("$");
							if (idx != -1) {
								className = className.substring(0, idx);
							}
						}
					} else if (count == 1) {
						methodName = temp;
						if (methodName.contains("(")) {
							methodName = methodName.substring(0, methodName.indexOf("("));
						}
					}
					count++;
				}

				Log.i("[" + cn + "] User selected debug result " + className);
				final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

				CodeEditor.toHighlight = new ArrayList<Tupple>();

				if (className != null && Global.sourceToLineMap.containsKey(className)) {
					final ClassNode cnode = Global.sourceToLineMap.get(className);
					final IFile file = cnode.getIfile();
					// CodeEditor.startHighlight = 100000;
					// CodeEditor.endHighlight = 100000;

					final List<LoopNode> loops = cnode.getLoops();

					// get the method node
					MethodNode mnode = getMethodNode(cnode.getmList(), methodName, null);
					if (mnode != null) {
						final int x = mnode.getLineNumber();
						final int y = mnode.getEndLineNumber();
						CodeEditor.toHighlight.add(new Tupple(x, y));
						checkIfInLoop(loops, x, y);
					}

					// additional highlights in case of anonymous class being
					// the rootcause
					mnode = getMethodNode(cnode.getmList(), methodName, mnode);
					if (mnode != null) {
						final int x = mnode.getLineNumber();
						final int y = mnode.getEndLineNumber();
						CodeEditor.toHighlight.add(new Tupple(x, y));
					}

					if (file != null) {
						try {
							final IEditorPart x = page.openEditor(new FileEditorInput(file), CodeEditor.ID);
						} catch (final PartInitException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						if (defect.patchString != null) {
							showMessage(defect.patchString);
						}

					} else {
						showMessage("Couldn't find the file object associated with this defect in the workspace");
					}

				} else {
					showMessage("Couldn't find the file object associated with this defect in the workspace");
				}

			}

			private void showGraph() {
				jFrame = new JFrame("Debug Explanation");
				jFrame.setSize(400, 400);
				jFrame.setLayout(new GridLayout(3, 1));
				jFrame.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosing(WindowEvent windowEvent) {
						System.exit(0);
					}
				});
				headerLabel = new JLabel("", JLabel.CENTER);
				statusLabel = new JLabel("", JLabel.CENTER);

				statusLabel.setSize(350, 100);

				controlPanel = new JPanel();
				controlPanel.setLayout(new FlowLayout());

				jFrame.add(headerLabel);
				jFrame.add(controlPanel);
				jFrame.add(statusLabel);
				jFrame.setVisible(true);

			}

			private boolean checkIfInLoop(List<LoopNode> loops, int startHighlight, int endHighlight) {
				for (final LoopNode l : loops) {
					if (l.getStartLine() <= startHighlight && l.getEndLine() >= endHighlight) {
						return true;
					}
				}

				return false;
			}

			private MethodNode getMethodNode(List<MethodNode> mList, String callerName, MethodNode lastNode) {

				for (final MethodNode m : mList) {
					if (m.getName().equals(callerName)) {
						if (lastNode != m) {
							return m;
						}
					}
				}

				return null;
			}

		};

	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();

	}

	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(), "EnergyDebugger Info", message);
	}

	public static void refresh() {
		viewer.refresh();

	}
}
