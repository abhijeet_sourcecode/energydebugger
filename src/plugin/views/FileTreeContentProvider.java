package plugin.views;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class FileTreeContentProvider implements ITreeContentProvider {

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub

	}

	/**
	 * Gets the children of the specified object
	 * 
	 * @param arg0
	 *            the parent object
	 * @return Object[]
	 */
	@Override
	public Object[] getChildren(Object arg0) {
		// System.out.println("in content provider , getChildren" + arg0);

		if (((File) arg0).isFile()) {
			return null;
		}

		final File[] temp = ((File) arg0).listFiles();
		final List<File> store = new ArrayList<File>();

		for (final File t : temp) {
			if (t.isDirectory()) {
				if (t.getName().toLowerCase().endsWith(".metadata")) {

				} else {
					store.add(t);
				}
			} else {
				if (t.getName().toLowerCase().endsWith(".java")) {
					store.add(t);
				}
			}
		}

		final File[] ret = new File[store.size()];
		store.toArray(ret);

		return ret;
	}

	/**
	 * Gets the parent of the specified object
	 * 
	 * @param arg0
	 *            the object
	 * @return Object
	 */
	@Override
	public Object getParent(Object arg0) {
		// System.out.println("in content provider , getParent" + arg0);
		return ((File) arg0).getParentFile();
	}

	/**
	 * Returns whether the passed object has children
	 * 
	 * @param arg0
	 *            the parent object
	 * @return boolean
	 */
	@Override
	public boolean hasChildren(Object arg0) {

		// System.out.println("in content provider , hasChildren" + arg0);

		final Object[] obj = getChildren(arg0);

		if (obj == null) {
			return false;
		}
		final List<File> store = new ArrayList<File>();

		for (final Object o : obj) {

			final File t = (File) o;
			if (t.isDirectory()) {
				if (t.getName().toLowerCase().endsWith(".metadata")) {

				} else {
					store.add(t);
				}
			} else {
				if (t.getName().toLowerCase().endsWith(".java")) {
					store.add(t);
				}
			}
		}

		if (store.size() > 0) {
			return true;
		} else {
			return false;
		}

		// return obj == null ? false : obj.length > 0;
	}

	/**
	 * Gets the root element(s) of the tree
	 * 
	 * @param arg0
	 *            the input data
	 * @return Object[]
	 */
	@Override
	public Object[] getElements(Object arg0) {

		// System.out.println("in content provider , getElements" + arg0);

		final File[] temp = ((File) arg0).listFiles();
		final List<File> store = new ArrayList<File>();

		for (final File t : temp) {
			if (t.isDirectory()) {
				if (t.getName().toLowerCase().endsWith(".metadata")) {

				} else {
					store.add(t);
				}
			} else {
				if (t.getName().toLowerCase().endsWith(".java")) {
					store.add(t);
				}
			}
		}

		final File[] ret = new File[store.size()];
		store.toArray(ret);

		return ret;
	}
}
