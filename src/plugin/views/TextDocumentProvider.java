package plugin.views;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.rules.FastPartitioner;
import org.eclipse.ui.editors.text.FileDocumentProvider;

public class TextDocumentProvider extends FileDocumentProvider {

	public static String ID = "plugin.editors.TextDocumentProvider";

	@Override
	protected IDocument createDocument(Object element) throws CoreException {
		final IDocument document = super.createDocument(element);

		if (document != null) {

			final IDocumentPartitioner partitioner = new FastPartitioner(new TextPartitionScanner(),
					TextPartitionScanner.PARTITION_TYPES);

			document.setDocumentPartitioner(partitioner);
			partitioner.connect(document);

		}

		return document;
	}

}
