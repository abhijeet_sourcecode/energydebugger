package plugin.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;

public class TextPartitionScanner extends RuleBasedPartitionScanner {

	// string constants for different partition types
	public final static String MULTILINE_COMMENT = "multiline_comment";
	public final static String SINGLELINE_COMMENT = "singleline_comment";
	public final static String STRING = "string";
	public final static String WHILE = "while";
	public final static String FOR = "for";
	public final static String IF = "if";
	public final static String ELSE = "else";
	public final static String[] PARTITION_TYPES = new String[] { MULTILINE_COMMENT, SINGLELINE_COMMENT, STRING, WHILE,
		FOR, IF, ELSE };

	/**
	 * Creates the partitioner and sets up the appropriate rules.
	 */
	public TextPartitionScanner() {
		super();

		final IToken multilinecomment = new Token(MULTILINE_COMMENT);
		final IToken singlelinecomment = new Token(SINGLELINE_COMMENT);
		final IToken string = new Token(STRING);

		final IToken IF_KEY = new Token(IF);

		final List rules = new ArrayList();

		// Add rule for single line comments.
		rules.add(new EndOfLineRule("//", singlelinecomment));

		// Add rule for strings and character constants.
		rules.add(new SingleLineRule("'", "'", string, '\\'));

		// Add rules for multi-line comments and javadoc.
		rules.add(new MultiLineRule("/*", "*/", multilinecomment, (char) 0, true));

		final WordRule wordRule = new WordRule(new MyWordDetector());

		// wordRule.addWord(IF, IF_KEY);
		//
		// rules.add(wordRule);

		final IPredicateRule[] result = new IPredicateRule[rules.size()];

		// System.out.println("size : " + rules.size());
		// System.out.println("length " + result.length);

		rules.toArray(result);
		setPredicateRules(result);
	}
}