package plugin.views;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.rules.PatternRule;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;

public class TextEditorConfiguration extends SourceViewerConfiguration {

	private static String[] fgKeywords = { "while", "for", "if", "else" };

	// @Override
	// public String getConfiguredDocumentPartitioning(ISourceViewer
	// sourceViewer) {
	// return TextPartitionScanner.PARTITIONING;
	// }

	@Override
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		return new String[] { IDocument.DEFAULT_CONTENT_TYPE, TextPartitionScanner.MULTILINE_COMMENT,
				TextPartitionScanner.SINGLELINE_COMMENT, TextPartitionScanner.STRING };
	}

	// private static ITokenScanner ITokenScanner = null;
	private final ColorManager colorManger;
	private ITokenScanner codeScanner;

	public TextEditorConfiguration(ColorManager colorManager) {
		this.colorManger = colorManager;
		codeScanner = createTokenScanner();

	}

	private ITokenScanner createTokenScanner() {
		final RuleBasedScanner scanner = new RuleBasedScanner();
		scanner.setRules(createRules());
		return scanner;
	}

	private IRule[] createRules() {
		final IToken tokenA = new Token(new TextAttribute(this.colorManger.getColor(ColorManager.STRING)));
		final IToken tokenB = new Token(new TextAttribute(this.colorManger.getColor(ColorManager.KEYWORD)));
		return new IRule[] { new PatternRule("/*", "*/", tokenA, '\\', false), new EndOfLineRule("-- ", tokenB) };

	}

	@Override
	public IPresentationReconciler getPresentationReconciler(ISourceViewer viewer) {

		final PresentationReconciler reconciler = new PresentationReconciler();
		reconciler.setDocumentPartitioning(getConfiguredDocumentPartitioning(viewer));

		final DefaultDamagerRepairer dr = new DefaultDamagerRepairer(codeScanner);
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		// dr = new
		// DefaultDamagerRepairer(JavaEditorExamplePlugin.getDefault().getJavaDocScanner());
		// reconciler.setDamager(dr, JavaPartitionScanner.JAVA_DOC);
		// reconciler.setRepairer(dr, JavaPartitionScanner.JAVA_DOC);

		// dr = new DefaultDamagerRepairer(new SingleTokenScanner(new
		// TextAttribute(
		// provider.getColor(JavaColorProvider.MULTI_LINE_COMMENT))));
		// reconciler.setDamager(dr,
		// JavaPartitionScanner.JAVA_MULTILINE_COMMENT);
		// reconciler.setRepairer(dr,
		// JavaPartitionScanner.JAVA_MULTILINE_COMMENT);

		return reconciler;
	}

	private ITokenScanner getCodeScanner() {
		if (codeScanner == null) {
			codeScanner = new RuleBasedScanner() {
				{
					setDefaultReturnToken(new Token(new TextAttribute(colorManger.getColor(ColorManager.GREY))));
				}
			};
		}
		return codeScanner;
	}

	private RuleBasedScanner textScanner = null;

	private ITokenScanner getScanner() {
		if (textScanner == null) {
			textScanner = new RuleBasedScanner() {
				{
					setDefaultReturnToken(new Token(new TextAttribute(colorManger.getColor(ColorManager.GREY))));
				}
			};
		}
		return textScanner;

	}

}
