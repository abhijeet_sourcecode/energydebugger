package nus.edu.sg;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nus.edu.sg.ds.Defect;
import nus.edu.sg.ds.DefectInfo;
import nus.edu.sg.ds.Hotspot;
import plugin.views.DebugResults;

public class Frequency {
	static Queue<Object> allLines = Collections.asLifoQueue(new LinkedList<Object>());
	static Queue<Object> params = Collections.asLifoQueue(new LinkedList<Object>());
	static Map<String, Info> db = new HashMap<>();

	static String marker = "outputfolderforinstrumentation\\extracted\\";
	private static float thresholdFrequency = 10;
	static long firstCall = 0;

	public static void main(String[] args) {

		Global.logPath = "C:\\Users\\workshop\\git\\energydebugger\\logfiles\\logfile_osm76.txt";
		test();
	}

	public static void test() {

		firstCall = 0;
		parse();
		final float f = getFreq();
		// System.out.println("observed freq " + f);
		if (f < 0) {
			return;
		} else {
			if (f < thresholdFrequency) {
				for (String x : db.keySet()) {

					if (db.get(x).occuredAt < firstCall) {

						final String params = db.get(x).params.toString();
						System.out.println(x + " \n  " + db.get(x).parent + " \n " + params);
						final String cn = db.get(x).parent;
						// hack
						final int idx = x.indexOf(";->");
						if (idx != -1) {
							x = x.substring(idx + 3);
						}

						final String mn = x;
						final long t = 0;
						final Hotspot hts = new Hotspot(cn, mn, t);
						hts.frequency = f;

						hts.caller = db.get(x).parent.replace("/", ".").replace("\\", ".").replace("_", ";->") + ":"
								+ x;
						hts.setSubType(7);
						hts.params = params;

						// System.out.println(hts.caller);
						final List<Defect> defects = new ArrayList<>();
						defects.add(hts);
						final DefectInfo d = new DefectInfo(defects, null, null);
						Global.defectList = new ArrayList<>();
						Global.defectList.add(d);

						Pipe.propagate(null);
						DebugResults.refresh();

						return;
					}
				}
			}
		}

	}

	private static float getFreq() {

		if (beats.size() <= 0) {
			return -1;
		}

		int count = 0;
		boolean flag = true;
		long last = 0;
		float totalTime = 0;
		for (final Long b : beats) {
			if (flag) {
				flag = false;
			} else {
				totalTime += (b - last) / 1000.0;
			}
			last = b;
			count++;
		}

		return totalTime / count;
	}

	private static void parse() {
		try {
			final BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(Global.logPath)));
			String line;
			while ((line = br.readLine()) != null) {

				if (line.contains("ResourceParameters")) {
					params.add(line);
				} else if (line.contains("requestLocationUpdates") && line.contains("DroidBox")) {
					final String key = getKey(line);
					final long time = getTime(line);
					if (!db.containsKey(key)) {
						final int tid = getTID(line);
						final String parent = getParent(tid);
						// System.out.println(key + ":" + parent);
						db.put(key, new Info(getKey(parent), getParams(), time));
					}
				} else if (line.contains("RPC_LOC_EVENT_SATELLITE_REPORT")) {
					final long time = getTime(line);
					if (time != -1) {
						beats.add(time);
						if (firstCall == 0) {
							firstCall = time;
						}
					}
				} else {
					allLines.add(line);
					// System.out.println("added to all " + line);
				}
			}

		} catch (final FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	static List<Long> beats = new ArrayList<Long>();

	private static long getTime(String line) {
		final int idx = line.indexOf(": ");
		if (idx != -1) {
			line = line.substring(0, idx);
		}
		final SimpleDateFormat parser = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");
		try {
			final Date date = parser.parse(line);
			return date.getTime();

		} catch (final ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}

	private static String getKey(String line) {

		int idx = line.lastIndexOf(":");
		if (idx != -1) {
			line = line.substring(idx + 1);
			line = line.trim();
		}
		idx = line.indexOf("=");
		if (idx != -1) {
			line = line.substring(0, idx);
			line = line.trim();
		}

		return line;
	}

	private static List getParams() {

		final List<Object> prm = new ArrayList<>();
		boolean flag = false;
		while (params.size() > 0) {
			String line = (String) params.remove();
			final int idx = line.indexOf("=");
			if (idx != -1) {
				line = line.substring(idx + 1);
				line = line.trim();
			}

			prm.add(line);
			if (flag) {
				return prm;
			}
			flag = true;
		}
		return null;
	}

	private static String getParent(int tid) {

		String line = null;
		boolean found = false;
		while (!found) {
			line = (String) allLines.remove();
			if (line.contains("EventHandler") && line.contains("=" + tid) && !line.contains("Finished_")) {
				found = true;
			}
		}
		if (found) {

			final int idx = line.indexOf(marker);
			if (idx != -1) {
				line = line.substring(idx + marker.length());
			}
			return line;
		} else {
			return null;
		}

	}

	private static int getTID(String line) {

		final Pattern pattern = Pattern.compile("V/DroidBox(.+?): ");
		final Matcher matcher = pattern.matcher(line);
		matcher.find();
		final String tid = matcher.group(1).replace("(", "").replace(")", "");
		int tid_i = -1;
		try {
			tid_i = Integer.parseInt(tid);
			// System.out.println(tid_i);
		} catch (final RuntimeException e) {
			return -1;
		}

		return tid_i;
		// System.exit(0);

		// final int idx = line.indexOf("V=");
		// String tid_s = null;
		// if (idx != -1) {
		// tid_s = line.substring(idx + 2);
		// }
		// int tid;
		// try {
		// tid = Integer.parseInt(tid_s);
		// } catch (final RuntimeException e) {
		// return -1;
		// }
		//
		// return tid;
	}
}
