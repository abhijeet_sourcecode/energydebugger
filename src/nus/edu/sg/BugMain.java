package nus.edu.sg;

import java.util.ArrayList;
import java.util.List;

import nus.edu.sg.ds.DNode;
import nus.edu.sg.ds.DTree;
import nus.edu.sg.ds.Defect;
import nus.edu.sg.ds.DefectInfo;
import nus.edu.sg.ds.EnergyAux;
import nus.edu.sg.ds.EnergyBug;
import nus.edu.sg.ds.GuidelineData;
import nus.edu.sg.ds.LogRecords;
//import nus.edu.sg.ds.DNode;
//import nus.edu.sg.ds.DTree;
//import nus.edu.sg.ds.EnergyBug;
//import nus.edu.sg.ds.GuidelineData;
//import nus.edu.sg.ds.LogRecords;
import nus.edu.sg.utility.GuidelineParser;
import plugin.views.DebugResults;
//import nus.edu.sg.ds.DNode;
//import nus.edu.sg.ds.DTree;
//import nus.edu.sg.ds.EnergyBug;
//import nus.edu.sg.ds.GuidelineData;
//import nus.edu.sg.ds.LogRecords;

public class BugMain {

	private static LogRecords logRecords;
	private static DTree dTree;

	private static GuidelineData thisResource;
	private static List<String> resPatterns;

	public static void main() {

		// should be put outside this function inside DebuggerMain
		final List<GuidelineData> serviceManagers = readServiceManagerListFromXML(Global.xmlFilePath);

		newALog(); // initialize a new log
		getLogRecords().readLogFromFile(Global.logPath); // read
		// log
		// from
		// a file

		if (getLogRecords().isLogEmpty()) {
			return;
		}

		for (final GuidelineData sm : serviceManagers) {

			thisResource = sm;
			setResPatterns(sm);
			final boolean debugFlag = getLogRecords().isResourceUsed(getResPatterns());

			if (debugFlag) {
				System.out.printf("Resource %s has been used in this application!\n\n", sm.getName());
			} else {
				continue;
			}

			setDTree(new DTree());
			getDTree().buildDTree(getLogRecords(), getResPatterns());

			// getDTree().tree2dot(Global.dotPath);

			// hotspotLocator();

			// Global.bugList = resourceBugLocator();
			Global.defectList = resourceBugLocator();
			// Global.printOut(Global.defectList);

			Pipe.propagate(getDTree());

			DebugResults.refresh();

		}

	}

	private static List<GuidelineData> readServiceManagerListFromXML(String xmlFile) {

		final GuidelineParser gp = new GuidelineParser();
		gp.parse(xmlFile);

		final List<GuidelineData> xml = gp.getxmlData();

		for (final GuidelineData d : xml) {

			if (d.getType().contains("Other")) {
				continue;
			}

			d.setSyscall1(adapt(d.getSyscall1()));
			d.setSyscall2(adapt(d.getSyscall2()));

		}

		return xml;
	}

	private static String adapt(String syscall) {

		if (syscall != null) {
			final int breakPoint = syscall.lastIndexOf(".");
			syscall = syscall.substring(0, breakPoint) + ";->" + syscall.substring(breakPoint + 1);
			return syscall.replace('.', '/');
		} else {
			return syscall;
		}
	}

	private static List<DefectInfo> resourceBugLocator() {
		// private static List<EnergyBug> resourceBugLocator() {
		// private static boolean checkServiceManagerBug(List<String> log,
		// GuidelineData service) {

		final List<DNode> resNodes = getDTree().getSysUseNodes();
		final List<Integer> bugLocationList = new ArrayList<Integer>();

		if (resNodes.size() == 0) {
			return null;
		}

		// boolean buggy = false;
		int k = getDTree().findNextNodeInPattern(thisResource.getSyscall1());

		while (k < resNodes.size()) {

			final int k1 = hasProperRelease(k, thisResource.getSyscall2());
			if (k1 < 0) {
				// buggy = true;
				bugLocationList.add(k);

				k = getDTree().findNextNodeInPattern(k, thisResource.getSyscall1());
			} else {
				k = getDTree().findNextNodeInPattern(k1, thisResource.getSyscall1());
			}
		}

		int subType;
		if (getDTree().getActivityNodes().size() == 0) {
			subType = 4; // immortality bug
		} else {
			subType = 1; // release bug
		}

		if (bugLocationList.size() > 0) {

			final List<EnergyBug> bugs = getDTree().getAllEnergyBugs(bugLocationList, subType);

			switch (subType) {
			case 4:
				System.out.println("Immortality bugs:");
				break;
			case 1:
				System.out.println("Resource release bugs:");
			}

			for (final EnergyBug b : bugs) {
				System.out.println(b);
			}
			// outputBugs(bugLocationList);
		}

		List<EnergyBug> bugs = getDTree().getAllEnergyBugs(bugLocationList, subType);

		// the following part is to detect "vacuous background service" (bug,
		// subtype = 3)

		final List<Integer> vbsBugList = new ArrayList<Integer>(); // vbs:
		// vacuous
		// background
		// services
		k = getDTree().findNextNodeInPattern(thisResource.getUseCalls());

		while (k >= 0 && k < resNodes.size()) {
			if (isVBSBug(k)) {
				vbsBugList.add(k);
			}

			k = getDTree().findNextNodeInPattern(k, thisResource.getUseCalls());
		}

		if (vbsBugList.size() > 0) {

			final List<EnergyBug> vbsBugs = getDTree().getAllEnergyBugs(vbsBugList, 3); // subtype
			// =
			// 3
			// (vacuous
			// background
			// service)

			System.out.println();
			System.out.println("Vacuous Background Service bugs:");
			for (final EnergyBug b : vbsBugs) {
				System.out.println(b);
			}
			System.out.println();
			// outputBugs(bugLocationList);
		}

		final int subtype = 3; // Vacuous Background Service bugs
		bugs = getDTree().addEnergyBugs(bugs, vbsBugList, subtype);

		final List<DefectInfo> defects = getDefectInfo(bugs, bugLocationList, vbsBugList);

		return defects;
	}

	private static List<DefectInfo> getDefectInfo(List<EnergyBug> bugs, List<Integer> bugLocationList,
			List<Integer> vbsBugList) {

		if (bugs.size() == 0) {
			return null;
		}

		final List<DefectInfo> defects = new ArrayList<DefectInfo>();

		int k = 0;

		if (bugLocationList.size() > 0) {

			if (bugs.get(k).getSubType() == 1) { // resource leak & wakelock bug

				for (final int acquireIndex : bugLocationList) {

					int onResumeBefore;
					int onResumeAfter; // if there is no more onResume,
					// onResumeAfter ==
					// getDTree().getActivityNodes().size()
					int onPause;
					final List<Defect> defectList = new ArrayList<Defect>();
					final List<EnergyAux> activityEnv = new ArrayList<EnergyAux>();
					final List<EnergyAux> resourceUse = new ArrayList<EnergyAux>();
					// EnergyAux eaNode;

					onResumeBefore = getDTree().findActivityOnResumeBeforeAcquire(acquireIndex);
					if (onResumeBefore < 0) {
						onResumeAfter = getDTree().findActivityOnResumeAfterAcquire(acquireIndex);
						if (onResumeAfter < getDTree().getActivityNodes().size()) {
							onResumeAfter = getDTree().findActivityOnResumeAfterActivity(onResumeBefore);
						}
					} else {
						onResumeAfter = getDTree().findActivityOnResumeAfterAcquire(acquireIndex);
					}

					// onResumeBefore =
					// getDTree().findActivityOnResumeBeforeAcquire(acquireIndex);
					// onResumeAfter =
					// getDTree().findActivityOnResumeAfterAcquire(acquireIndex);
					onPause = getDTree().findActivityOnPauseBetween(onResumeBefore, onResumeAfter);

					// add activity aux node info for onResumeBefore
					if (onResumeBefore >= 0 && onResumeBefore < getDTree().getActivityNodes().size()) {
						activityEnv.add(getEnergyAuxActivityNode(onResumeBefore));
					}

					// add activity aux node info for onPause
					if (onPause < getDTree().getActivityNodes().size()) {
						activityEnv.add(getEnergyAuxActivityNode(onPause));
					}

					// add activity aux node info for onResumeAfter, if there is
					// an onResumeAfter
					if (onResumeAfter < getDTree().getActivityNodes().size()) {
						activityEnv.add(getEnergyAuxActivityNode(onResumeAfter));
					}

					// add resource usage nodes in the context (within the
					// activity life cycle)
					// time between onResumeBefore and onPause
					setEnergyAuxResourceUseNode(resourceUse, onResumeBefore, onPause);
					defectList.add(bugs.get(k));
					defects.add(new DefectInfo(defectList, activityEnv, resourceUse));

					k++;
				}
			} else { // if (bugs.get(k).getSubType() == 3) // immortality bug
				final List<Defect> defectList = new ArrayList<Defect>();
				final List<EnergyAux> activityEnv = new ArrayList<EnergyAux>();
				final List<EnergyAux> resourceUse = new ArrayList<EnergyAux>();
				// EnergyAux eaNode;
				for (final int acquireIndex : bugLocationList) {
					defectList.add(bugs.get(k));
					k++;
				}
				setEnergyAuxAllResourceUseNode(resourceUse);

				defects.add(new DefectInfo(defectList, activityEnv, resourceUse));
			}

		}

		int lastOnResumeBefore = -1;
		int onResumeBefore;
		int onResumeAfter; // if there is no more onResume, onResumeAfter ==
		// getDTree().getActivityNodes().size()
		int onPause;
		final List<Defect> defectList = new ArrayList<Defect>();
		final List<EnergyAux> activityEnv = new ArrayList<EnergyAux>();
		final List<EnergyAux> resourceUse = new ArrayList<EnergyAux>();
		// EnergyAux eaNode;

		for (final int acquireIndex : vbsBugList) { // vacuous background
			// services

			onResumeBefore = getDTree().findActivityOnResumeBeforeAcquire(acquireIndex);

			if (onResumeBefore == lastOnResumeBefore) {
				defectList.add(bugs.get(k));
				k++;
				continue;
			}

			if (lastOnResumeBefore != -1) {
				defects.add(new DefectInfo(defectList, activityEnv, resourceUse));

				defectList.clear();
				activityEnv.clear();
				resourceUse.clear();
			}

			onResumeAfter = getDTree().findActivityOnResumeAfterAcquire(acquireIndex);
			onPause = getDTree().findActivityOnPauseBetween(onResumeBefore, onResumeAfter);

			// add activity aux node info for onResumeBefore
			activityEnv.add(getEnergyAuxActivityNode(onResumeBefore));

			// add activity aux node info for onPause
			if (onPause < getDTree().getActivityNodes().size()) {
				activityEnv.add(getEnergyAuxActivityNode(onPause));
			}

			// add activity aux node info for onResumeAfter, if there is an
			// onResumeAfter
			if (onResumeAfter < getDTree().getActivityNodes().size()) {
				activityEnv.add(getEnergyAuxActivityNode(onResumeAfter));
			}

			// add resource usage nodes in the context (within the activity life
			// cycle)
			// time between onPause and onResumeAfter
			setEnergyAuxResourceUseNode(resourceUse, onPause, onResumeAfter);

			defectList.add(bugs.get(k));

			lastOnResumeBefore = onResumeBefore;
			k++;
		}

		if (vbsBugList.size() > 0) {
			defects.add(new DefectInfo(defectList, activityEnv, resourceUse));
		}

		return defects;
	}

	private static List<EnergyAux> setEnergyAuxResourceUseNode(List<EnergyAux> resourceUse, int beforeActivity,
			int afterActivity) {
		// afterActivity may not exist (afterActivity ==
		// getDTree().getActivityNodes().size())
		boolean toEnd = false;

		if (beforeActivity < 0) {
			beforeActivity = 0;
		}
		final long beforeTime = getDTree().getActivityNodes().get(beforeActivity).getTimeStamp();
		long afterTime = 0;

		if (afterActivity == getDTree().getActivityNodes().size()) {
			toEnd = true;
		} else {
			afterTime = getDTree().getActivityNodes().get(afterActivity).getTimeStamp();
		}

		int i = 0;
		final List<DNode> useNodes = getDTree().getSysUseNodes();

		while (i < useNodes.size()) {

			final long thisTimeStamp = useNodes.get(i).getTimeStamp();
			if (!toEnd && thisTimeStamp > afterTime) {
				break;
			}

			if (thisTimeStamp >= beforeTime) {
				resourceUse.add(getEnergyAuxResourceUseNode(i));
			}

			i++;
		}

		return resourceUse;
	}

	private static List<EnergyAux> setEnergyAuxAllResourceUseNode(List<EnergyAux> resourceUse) {
		// afterActivity may not exist (afterActivity ==
		// getDTree().getActivityNodes().size())

		int i = 0;
		final List<DNode> useNodes = getDTree().getSysUseNodes();

		while (i < useNodes.size()) {
			resourceUse.add(getEnergyAuxResourceUseNode(i));
			i++;
		}

		return resourceUse;
	}

	public static EnergyAux getEnergyAuxActivityNode(int index) {

		DNode activityNode;
		DNode callerNode;

		activityNode = getDTree().getActivityNodes().get(index);
		callerNode = activityNode.getParent();

		return new EnergyAux(callerNode.getFile() + ";->" + callerNode.getMethod(), activityNode.getFile(),
				activityNode.getMethod(), activityNode.getTimeStamp());
	}

	public static EnergyAux getEnergyAuxResourceUseNode(int index) {

		DNode useNode;
		DNode callerNode;

		useNode = getDTree().getResUseNode(index);
		callerNode = useNode.getParent();

		return new EnergyAux(callerNode.getFile() + ";->" + callerNode.getMethod(), useNode.getFile(),
				useNode.getMethod(), useNode.getTimeStamp());
	}

	private static void outputBugs(List<Integer> bugList) {

		System.out
		.printf("[Engergy Bug Warning] no energy release statement is corresponding to the following energy acquire statements:\n");

		for (final Integer i : bugList) {

			final DNode bugNode = getDTree().getSysUseNodes().get(i);

			System.out.printf("Time Stampe (%d): %s->%s\n", bugNode.getTimeStamp(), bugNode.getFile(),
					bugNode.getMethod());

			// out.format("Engergy Bug Warning: no energy release statement is corresponding to the following energy acquire statement @ line %d:\n",
			// acquireLine);
			// out.format("  %s\n\n", log.get(acquireLine));

		}
		System.out.println();

	}

	private static boolean isVBSBug(int useLine) { // vacuous background service
		// bug

		final long useTimeStamp = getDTree().getSysUseNodes().get(useLine).getTimeStamp();

		// find the activity method right before useTimeStamp: beforeActivity
		// find the activity method right after useTimeStapm: afterActivity (not
		// implemented for now)
		// if (beforeActivity is _onPause && afterActivity is _onResume) return
		// true else false

		int i = getDTree().getActivityNodes().size() - 1;

		while (i >= 0) {

			final DNode beforeActivityNode = getDTree().getActivityNodes().get(i);
			if (beforeActivityNode.getTimeStamp() < useTimeStamp) {

				if (beforeActivityNode.getMethodName().contains("onPause")) {
					return true;
				}

				return false;
			}
			i--;
		}

		return false;
	}

	private static int hasProperRelease(int acquireLine, String releasePattern) {
		// private static boolean hasProperRelease(int acquireLine, List<String>
		// log, GuidelineData service) {

		// findRegisteredServiceInfo(log.get(acquireLine));

		// System.out.printf("service register at %d: \n", acquireLine);
		// System.out.printf("  %s\n", log.get(acquireLine));

		int resumeLine;
		int pauseLine;
		int onResumeBefore;

		// if (thisResource.getName().contains("wakelocks") ||
		// thisResource.getName().contains("wifi manager - acquire")
		if (thisResource.getName().contains("camera manager - open/release")
				|| thisResource.getName().contains("MediaPlayer - prepareAsync/release")
				|| thisResource.getName().contains("MediaPlayer - prepare/release")) {
			// search globally after the acquireLine
			resumeLine = getLogRecords().size();
			pauseLine = -1;
		} else {

			onResumeBefore = getDTree().findActivityOnResumeBeforeAcquire(acquireLine);
			// if (onResumeBefore < 0 || onResumeBefore ==
			// getDTree().getActivityNodes().size())
			// onResumeBefore =
			// getDTree().findActivityOnCreateBeforeAcquire(acquireLine);
			//
			// resumeLine =
			// getDTree().findActivityOnResumeAfterAcquire(acquireLine);

			if (onResumeBefore < 0) {
				resumeLine = getDTree().findActivityOnResumeAfterAcquire(acquireLine);
				if (resumeLine < getDTree().getActivityNodes().size()) {
					resumeLine = getDTree().findActivityOnResumeAfterActivity(resumeLine);
				}
			} else {
				resumeLine = getDTree().findActivityOnResumeAfterAcquire(acquireLine);
			}

			onResumeBefore = getDTree().findActivityOnResumeBeforeAcquire(acquireLine);
			if (onResumeBefore < 0 || onResumeBefore == getDTree().getActivityNodes().size()) {
				onResumeBefore = getDTree().findActivityOnCreateBeforeAcquire(acquireLine);
			}

			// if (onResumeBefore >= 0 && onResumeBefore <
			// getDTree().getActivityNodes().size())
			// pauseLine = getDTree().findActivityOnPauseBetween(onResumeBefore,
			// resumeLine);
		}

		int releaseLine;

		// if (pauseLine > 0) // onPause exists
		// releaseLine = findUnregisterBetween(pauseLine+1, resumeLine-1, log,
		// service);
		// else // onPause doesn't exist

		releaseLine = getDTree().findUnregisterPatternBetween(acquireLine, resumeLine, releasePattern);

		return releaseLine;

	}

	public static void newALog() {
		logRecords = new LogRecords();
	}

	public static LogRecords getLogRecords() {
		return logRecords;
	}

	public static DTree getDTree() {
		return dTree;
	}

	public static void setDTree(DTree t) {
		dTree = t;
	}

	private static void setResPatterns(GuidelineData sm) {

		final List<String> ps = new ArrayList<String>();

		if (sm.getSyscall1() != null && !sm.getSyscall1().equals("")) {
			ps.add(sm.getSyscall1());
		}
		if (sm.getSyscall2() != null && !sm.getSyscall2().equals("")) {
			ps.add(sm.getSyscall2());
		}

		final List<String> usePatterns = sm.getUseCalls();
		if (usePatterns != null) {
			for (final String s : usePatterns) {
				ps.add(s);
			}
		}

		resPatterns = ps;
	}

	private static List<String> getResPatterns() {
		return resPatterns;
	}

}
