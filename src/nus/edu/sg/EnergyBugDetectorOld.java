package nus.edu.sg;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

import nus.edu.sg.ds.GuidelineData;
import nus.edu.sg.utility.GuidelineParser;

public class EnergyBugDetectorOld {

	private static String xmlFilePath = "C:/Users/dcsghf/workspace/EnergyBugDetector/input/guideline.xml";
	private static String logPath = "C:\\Users\\dcsghf\\workspace\\EFGGen\\workingDir\\logcat\\";
	// private static String logFileName = "logAripucaCombine.txt";
	// private static String logFileName = "logMontrealtransitCombine.txt";
	// private static String logFileName = "logFoodCourt.txt";
	// private static String logFileName = "logOmnidroid.txt";
	// private static String logFileName = "logPlumber.txt";
	// private static String logFileName = "logSensorTester.txt";
	// private static String logFileName = "logTachometer.txt";
	private static String logFileName = "logUserhashNoReboot.txt";

	// private static String outputFileName = "bugReportAripuca.txt";
	// private static String outputFileName = "bugReportMontrealtransit.txt";
	// private static String outputFileName = "bugReportFoodCourt.txt";
	// private static String outputFileName = "bugReportOmnidroid.txt";
	// private static String outputFileName = "bugReportPlumber.txt";
	// private static String outputFileName = "bugReportSensorTester.txt";
	// private static String outputFileName = "bugReportTachometer.txt";
	private static String outputFileName = "bugUserhash.txt";

	private static Formatter out;

	private static String activity;
	private static String serviceManager;
	private static String serviceListener;

	public static void main(String[] args) {

		final List<GuidelineData> serviceManagers = readServiceManagerListFromXML(xmlFilePath);
		final List<String> log = readLogFromFile(logPath + logFileName);

		try {
			out = new Formatter(new File(logPath + outputFileName));
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
			out = null;
		}

		// checkLocationManagerBug(log);
		// checkRegisteredServiceManagerBug(log);

		for (final GuidelineData sm : serviceManagers) {
			checkServiceManagerBug(log, sm); // acquire
		}

		out.close();
	}

	private static List<GuidelineData> readServiceManagerListFromXML(String xmlFile) {

		final GuidelineParser gp = new GuidelineParser();
		gp.parse(xmlFile);

		final List<GuidelineData> xml = gp.getxmlData();

		for (final GuidelineData d : xml) {

			if (d.getType().contains("Other")) {
				continue;
			}

			d.setSyscall1(adapt(d.getSyscall1()));
			d.setSyscall2(adapt(d.getSyscall2()));

		}

		return xml;
	}

	private static String adapt(String syscall) {

		if (syscall != null) {
			final int breakPoint = syscall.lastIndexOf(".");
			syscall = syscall.substring(0, breakPoint) + ";->" + syscall.substring(breakPoint + 1);
			return syscall.replace('.', '/');
		} else {
			return syscall;
		}
	}

	private static boolean checkServiceManagerBug(List<String> log, GuidelineData service) {

		final List<Integer> lines = findAllLinesByMethod(log, service.getSyscall1());

		if (lines.size() == 0) {
			return false;
		}

		System.out.println("*************************************************************");
		System.out.printf("Energy Debugging: %s\n", service.getName());
		System.out.println("*************************************************************\n");
		out.format("*************************************************************\n");
		out.format("Energy Debugging: %s\n", service.getName());
		out.format("*************************************************************\n\n");

		boolean buggy = false;
		int k = 0;

		while (k < lines.size()) {

			buggy = buggy | !hasProperRelease(lines.get(k), log, service);
			// System.out.println();

			k++;
		}

		return buggy;
	}

	private static boolean hasProperRelease(int acquireLine, List<String> log, GuidelineData service) {

		findRegisteredServiceInfo(log.get(acquireLine));

		// System.out.printf("service register at %d: \n", acquireLine);
		// System.out.printf("  %s\n", log.get(acquireLine));

		int resumeLine;
		int pauseLine;

		if (service.getName().contains("wakelocks") || service.getName().contains("wifi manager - acquire")
				|| service.getName().contains("camera manager - open/release")
				|| service.getName().contains("MediaPlayer - prepareAsync/release")
				|| service.getName().contains("MediaPlayer - prepare/release")) {
			// search globally after the acquireLine
			resumeLine = log.size();
			pauseLine = -1;
		} else {
			findActivityOnResumeBeforeAcquire(acquireLine, log);
			resumeLine = findActivityOnResumeAfterAcquire(acquireLine, log);
			pauseLine = findActivityOnPauseBetween(acquireLine + 1, resumeLine - 1, log);
		}

		int releaseLine;

		// if (pauseLine > 0) // onPause exists
		// releaseLine = findUnregisterBetween(pauseLine+1, resumeLine-1, log,
		// service);
		// else // onPause doesn't exist
		releaseLine = findUnregisterBetween(acquireLine + 1, resumeLine - 1, log, service);

		if (releaseLine > 0) {
			return true;
		} else {
			System.out
			.printf("Engergy Bug Warning: no energy release statement is corresponding to the following energy acquire statement @ line %d:\n",
					acquireLine);
			System.out.printf("  %s\n\n", log.get(acquireLine));
			out.format(
					"Engergy Bug Warning: no energy release statement is corresponding to the following energy acquire statement @ line %d:\n",
					acquireLine);
			out.format("  %s\n\n", log.get(acquireLine));

			return false;
		}

	}

	private static int findUnregisterBetween(int i, int j, List<String> log, GuidelineData service) {

		final String unService = service.getSyscall2();

		int k = i;

		while (k <= j) {

			if (log.get(k).contains(unService)) {
				// System.out.printf("%s at %d: \n", unService, k);
				// System.out.printf("  %s\n", log.get(k));

				break;
			}

			k++;
		}

		return k <= j ? k : -1;
	}

	private static void findRegisteredServiceInfo(String line) {

		final String[] fields = line.split(" ");

		// field[3] contains info of service manager and listener
		// will be helpful when source code is used
		setServiceManager(fields[3].substring(0, fields[3].indexOf(";->")));

		// skip the service for now unless the source code is used
	}

	private static int findActivityOnPauseBetween(int begin, int end, List<String> log) {

		int i = begin;

		while (i <= end) {

			if (log.get(i).contains(getActivity() + "_onPause")) {
				// System.out.printf("%s: onPause at %d: \n", getActivity(), i);
				// System.out.printf("  %s\n", log.get(i));

				break;
			}

			i++;
		}

		if (i > end) {
			return -1;
		} else {
			return i;
		}
	}

	private static int findActivityOnResumeAfterAcquire(int acquireLine, List<String> log) {

		int i = acquireLine + 1;

		while (i < log.size()) {

			if (log.get(i).contains(getActivity() + "_onResume")) {
				// System.out.printf("next %s: onResume begins at %d: \n",
				// getActivity(), i);
				// System.out.printf("  %s\n", log.get(i));

				break;
			}

			i++;
		}

		return i;
	}

	private static int findActivityOnResumeBeforeAcquire(int acquireLine, List<String> log) {

		int i = acquireLine - 1;

		while (i >= 0) {

			if (!log.get(i).contains("Finished_onResume") && log.get(i).contains("_onResume")) {

				final int p2 = log.get(i).indexOf("_onResume");
				final int p1 = log.get(i).lastIndexOf("\\", p2);
				setActivity(log.get(i).substring(p1 + 1, p2));

				// System.out.printf("%s: onResume begins at %d: \n",
				// getActivity(), i);
				// System.out.printf("  %s\n", log.get(i));
				break;
			}

			i--;
		}

		return i;
	}

	private static List<Integer> findAllLinesByMethod(List<String> log, String method) {

		final List<Integer> intArr = new ArrayList<Integer>();

		for (int i = 0; i < log.size(); i++) {

			if (log.get(i).contains(method)) {
				intArr.add(i);
			}
		}

		return intArr;
	}

	private static List<String> readLogFromFile(String fullFilePath) {

		final List<String> logStrs = new ArrayList<String>();

		try {
			final BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fullFilePath)));

			String line;
			while ((line = br.readLine()) != null) {
				logStrs.add(line);
			}
			br.close();

		} catch (final IOException e) {
			System.err.println("Error: " + fullFilePath + " does not exist!");
			System.exit(0);
		}

		return logStrs;
	}

	public static String getActivity() {
		return activity;
	}

	public static void setActivity(String a) {
		activity = a;
	}

	public static String getServiceManager() {
		return serviceManager;
	}

	public static void setServiceManager(String s) {
		serviceManager = s;
	}

	public static String getServiceListener() {
		return serviceListener;
	}

	public static void setServiceListener(String s) {
		serviceListener = s;
	}

}
