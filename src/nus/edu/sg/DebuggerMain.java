package nus.edu.sg;

public class DebuggerMain {

	public static void main(String[] args) {

		final long startTime = System.currentTimeMillis();
		try {
			BugMain.main();
			System.out.println("Bug main completed");
		} catch (final RuntimeException e) {
			System.out.println("Exception during bug testing");
		}
		try {
			HotSpotMain.main();
			System.out.println("Hotspot main completed");
		} catch (final RuntimeException e) {
			System.out.println("Exception during hotspot testing");
		}
		try {
			Frequency.test();
		} catch (final RuntimeException e) {
			System.out.println("Exception during frequency testing");
		}
		final long endTime = System.currentTimeMillis();

		final long difference = endTime - startTime;

		System.out.println("Elapsed time in milliseconds: " + difference);
	}

}
