package nus.edu.sg;

import java.io.IOException;

public class Log {

	public static void d(String msg) {
		if (Global.verbosity <= 0) {
			System.out.println(msg);
		}

	}

	public static void i(String msg) {
		if (Global.verbosity <= 1) {
			System.out.println(msg);
			if (Global.out != null) {
				try {
					Global.out.write(msg + "\n");
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public static void e(String msg) {
		System.err.println(msg);
		if (Global.out != null) {
			try {
				Global.out.write(msg + "\n");
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
