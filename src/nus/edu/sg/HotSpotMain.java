package nus.edu.sg;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import nus.edu.sg.ds.DNode;
import nus.edu.sg.ds.DTree;
import nus.edu.sg.ds.Defect;
import nus.edu.sg.ds.DefectInfo;
import nus.edu.sg.ds.EnergyAux;
import nus.edu.sg.ds.GuidelineData4Hotspot;
import nus.edu.sg.ds.Hotspot;
import nus.edu.sg.ds.LogRecords;
//import nus.edu.sg.ds.DNode;
//import nus.edu.sg.ds.DTree;
//import nus.edu.sg.ds.DefectInfo;
//import nus.edu.sg.ds.EnergyAux;
//import nus.edu.sg.ds.GuidelineData4Hotspot;
//import nus.edu.sg.ds.Hotspot;
//import nus.edu.sg.ds.LogRecords;
import nus.edu.sg.utility.GuidelineParser4Hotspot;
import plugin.views.DebugResults;

public class HotSpotMain {

	// private static List<MatchedString> log;
	private static LogRecords logRecords;
	private static DTree dTree;

	private static GuidelineData4Hotspot thisResource;
	private static List<String> resPatterns;
	private static List<String> usePatterns;
	private static Hashtable<String, String> pairedPatterns;

	public static void main() {

		// should be put outside this function inside DebuggerMain
		final List<GuidelineData4Hotspot> serviceManagers = readServiceManagerListFromXML(Global.hotspotXmlFilePath);
		newALog();
		getLogRecords().readLogFromFile(Global.logPath); // set a new log

		if (getLogRecords().isLogEmpty()) {
			return;
		}

		for (final GuidelineData4Hotspot sm : serviceManagers) {

			thisResource = sm;
			setPatterns(thisResource);
			final boolean debugFlag = getLogRecords().isResourceUsed(resPatterns);

			if (debugFlag) {
				System.out.printf("Resource %s has been used in this application!\n\n", sm.getName());
			} else {
				continue;
			}

			// long starttime = getLogRecords().getInstantTime(0);
			// int end = getLogRecords().size();
			// long endtime = getLogRecords().getInstantTime(end-1);
			// long runtime = endtime - starttime;

			setDTree(new DTree());
			getDTree().buildDTree(getLogRecords(), getResPatterns());

			// getDTree().tree2dot(outputFile+sm.getType()+".dot");

			// hotspotLocator();
			Global.defectList = hotspotLocator();

			Pipe.propagate(getDTree());

			DebugResults.refresh();
		}
	}

	private static List<DefectInfo> hotspotLocator() {

		if (thisResource.isExclusive()) {
			return hotspotLocatorExclusiveRes();
		} else {
			return hotspotLocatorSharedRes();
		}
	}

	private static List<DefectInfo> hotspotLocatorSharedRes() {

		int thisNode = getDTree().findNextNodeInPattern(getUsePatterns());

		if (thisNode < 0 || thisNode == getDTree().getSysUseNodes().size()) {
			return null;
		}

		int nextNode = getDTree().findNextNodeInPattern(thisNode, getUsePatterns());
		final List<Hotspot> hotspotTable = new ArrayList<Hotspot>();

		final List<DefectInfo> defects = new ArrayList<DefectInfo>();
		final List<Integer> resourceUseIndexList = new ArrayList<Integer>();

		while (nextNode > 0) {

			resourceUseIndexList.add(thisNode);

			getDTree().hotspotTraversal(thisNode, nextNode, hotspotTable);
			thisNode = nextNode;
			nextNode = getDTree().findNextNodeInPattern(thisNode, getUsePatterns());
		}

		resourceUseIndexList.add(thisNode);

		for (final Hotspot h : hotspotTable) {
			System.out.println(h);
		}
		// outputHotspot(hotspotTable);

		if (!hotspotTable.isEmpty()) {
			addDefectInfo(defects, hotspotTable, resourceUseIndexList);
		}

		return defects;
	}

	private static List<DefectInfo> hotspotLocatorExclusiveRes() {

		int holdNode = getDTree().findNextNodeInPattern(thisResource.getBegincall());
		if (holdNode < 0 || holdNode == getDTree().getSysUseNodes().size()) {
			return null;
		}

		int releaseNode = getDTree().findNextNodeInPattern(holdNode, thisResource.getEndcall());
		if (releaseNode < 0 || releaseNode == getDTree().getSysUseNodes().size()) {
			System.err.printf("Error: resource %s is not properly released!\n", thisResource.getType());
			return null;
			// System.exit(1);
		}

		final List<DefectInfo> defects = new ArrayList<DefectInfo>();
		final List<Integer> resourceUseIndexList = new ArrayList<Integer>();

		// final Hashtable<String, Hotspot> hashTable = new Hashtable<String,
		// Hotspot>();
		// change hashTable to ArrayList,
		final List<Hotspot> hotspotTable = new ArrayList<Hotspot>();
		while (holdNode > 0) {

			resourceUseIndexList.add(holdNode);

			int thisNode = getDTree().findNextNodeInPattern(holdNode, getUsePatterns());
			while (thisNode > 0 && thisNode < releaseNode) {

				resourceUseIndexList.add(thisNode);
				getDTree().hotspotTraversal(holdNode, thisNode, hotspotTable);
				final int stopNode = getDTree().findPairedStopNode(thisNode, thisResource);

				if (stopNode < 0 || stopNode == getDTree().getSysUseNodes().size()) {
					holdNode = thisNode;
				} else {
					resourceUseIndexList.add(stopNode);
					holdNode = stopNode;
				}

				thisNode = getDTree().findNextNodeInPattern(holdNode, getUsePatterns());
			}

			resourceUseIndexList.add(releaseNode);
			getDTree().hotspotTraversal(holdNode, releaseNode, hotspotTable);

			if (hotspotTable.size() > 0) {
				addDefectInfo(defects, hotspotTable, resourceUseIndexList);
				outputHotspot(hotspotTable);
			}

			hotspotTable.clear();
			resourceUseIndexList.clear();

			holdNode = getDTree().findNextNodeInPattern(releaseNode, thisResource.getBegincall());
			if (holdNode < 0 || holdNode >= getDTree().getSysUseNodes().size()) {
				break;
			}

			releaseNode = getDTree().findNextNodeInPattern(holdNode, thisResource.getEndcall());
			if (releaseNode < 0 || releaseNode >= getDTree().getSysUseNodes().size()) {
				System.err.printf("Error: resource %s is not properly released!\n", thisResource.getType());
				break;
				// System.exit(1);
			}
		}

		if (hotspotTable.size() > 0) {
			outputHotspot(hotspotTable);
		}
		return defects;
	}

	private static void addDefectInfo(List<DefectInfo> defects, List<Hotspot> hotspotTable,
			List<Integer> resourceUseList) {

		final List<Defect> hotspots = new ArrayList<Defect>();
		hotspots.addAll(hotspotTable);

		int onResumeBefore;
		int onResumeAfter; // if there is no more onResume, onResumeAfter ==
		// getDTree().getActivityNodes().size()
		int onPause;
		final List<EnergyAux> activityEnv = new ArrayList<EnergyAux>();

		onResumeBefore = getDTree().findActivityOnResumeBeforeAcquire(resourceUseList.get(0));
		if (onResumeBefore < 0 || onResumeBefore == getDTree().getActivityNodes().size()) {
			onResumeBefore = getDTree().findActivityOnCreateBeforeAcquire(resourceUseList.get(0));
		}

		onResumeAfter = getDTree().findActivityOnResumeAfterAcquire(resourceUseList.get(resourceUseList.size() - 1));
		if (onResumeAfter < 0 || onResumeAfter == getDTree().getActivityNodes().size()) {
			onResumeBefore = getDTree().findActivityOnStopAfterAcquire(resourceUseList.get(0));
		}

		onPause = getDTree().findActivityOnPauseBetween(onResumeBefore, onResumeAfter);

		// add activity aux node info for onResumeBefore
		if (onResumeBefore >= 0 && onResumeBefore < getDTree().getActivityNodes().size()) {
			activityEnv.add(getEnergyAuxActivityNode(onResumeBefore));
		}

		// add activity aux node info for onPause
		if (onPause >= 0 && onPause < getDTree().getActivityNodes().size()) {
			activityEnv.add(getEnergyAuxActivityNode(onPause));
		}

		// add activity aux node info for onResumeAfter, if there is an
		// onResumeAfter
		if (onResumeAfter >= 0 && onResumeAfter < getDTree().getActivityNodes().size()) {
			activityEnv.add(getEnergyAuxActivityNode(onResumeAfter));
		}

		// add resource usage nodes in the context (within the activity life
		// cycle)
		// time between onResumeBefore and onPause
		final List<EnergyAux> resourceAuxList = new ArrayList<EnergyAux>();

		setEnergyAuxResourceUseList(resourceAuxList, resourceUseList);
		defects.add(new DefectInfo(hotspots, activityEnv, resourceAuxList));
	}

	private static void setEnergyAuxResourceUseList(List<EnergyAux> resourceAuxList, List<Integer> resourceUseList) {

		for (final int i : resourceUseList) {
			resourceAuxList.add(getEnergyAuxResourceUseNode(i));
		}
	}

	public static EnergyAux getEnergyAuxActivityNode(int index) {

		DNode activityNode;
		DNode callerNode;

		activityNode = getDTree().getActivityNodes().get(index);
		callerNode = activityNode.getParent();

		return new EnergyAux(callerNode.getFile() + ";->" + callerNode.getMethod(), activityNode.getFile(),
				activityNode.getMethod(), activityNode.getTimeStamp());
	}

	public static EnergyAux getEnergyAuxResourceUseNode(int index) {

		DNode useNode;
		DNode callerNode;

		useNode = getDTree().getResUseNode(index);
		callerNode = useNode.getParent();

		return new EnergyAux(callerNode.getFile() + ";->" + callerNode.getMethod(), useNode.getFile(),
				useNode.getMethod(), useNode.getTimeStamp());
	}

	private static void outputHotspot(List<Hotspot> hotspotTable) {

		System.out.printf("Warning! potential hotspot locations:\n");
		for (final Hotspot s : hotspotTable) {
			System.out.println(s);
		}

	}

	private static void setPatterns(GuidelineData4Hotspot sm) {

		final List<String> patterns = new ArrayList<String>();
		final List<String> uPatterns = new ArrayList<String>();

		patterns.add("L" + sm.getBegincall());
		for (final String s : sm.getUsagecalls()) {
			patterns.add(s);
			uPatterns.add(s);
		}
		if (sm.getEndcall() != null && !sm.getEndcall().trim().equals("")) {
			patterns.add(sm.getEndcall());
		}
		setPairedPatterns(sm.getPairedUseCalls());

		for (final String s : sm.getPairedUseCalls().keySet()) {
			patterns.add(s);
			uPatterns.add(s);
			patterns.add(sm.getPairedUseCalls().get(s));
			uPatterns.add(sm.getPairedUseCalls().get(s));
		}

		resPatterns = patterns;
		usePatterns = uPatterns;
	}

	private static List<String> getResPatterns() {
		return resPatterns;
	}

	private static List<String> getUsePatterns() {
		return usePatterns;
	}

	public static Hashtable<String, String> getPairedPatterns() {
		return pairedPatterns;
	}

	public static void setPairedPatterns(Hashtable<String, String> pairedPatterns) {
		HotSpotMain.pairedPatterns = pairedPatterns;
	}

	private static List<GuidelineData4Hotspot> readServiceManagerListFromXML(String xmlFile) {

		final GuidelineParser4Hotspot gp = new GuidelineParser4Hotspot();
		gp.parse(xmlFile);
		// gp.showxmlData();

		final List<GuidelineData4Hotspot> xml = gp.getxmlData();

		return xml;
	}

	public static void newALog() {
		logRecords = new LogRecords();
	}

	public static LogRecords getLogRecords() {
		return logRecords;
	}

	public static DTree getDTree() {
		return dTree;
	}

	public static void setDTree(DTree t) {
		dTree = t;
	}
}
