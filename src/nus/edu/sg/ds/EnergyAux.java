package nus.edu.sg.ds;

import nus.edu.sg.Global;

public class EnergyAux {
	private String file; // the full path/class where the method <M> is defined
	private String method; // the method name <M>
	private String caller; // the full path/class and method name of the caller
	private long timeStamp;

	public EnergyAux(String cr, String cn, String mn, long time) {
		setCaller(cr);
		setFile(cn);
		setMethod(mn);
		setTimeStamp(time);
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {

		String temp = caller;
		temp = temp.replace("/", ".").replace("\\", ".");
		if (temp.contains(Global.instrumentationDir)) {
			// temp = temp.substring(temp.indexOf(Global.instrumentationDir) +
			// Global.instrumentationDir.length());
			temp = Global.getWithoutDir(temp);
		}

		this.caller = temp;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Override
	public String toString() {
		final String mt = method;
		final String cl = caller;
		return cl + ":" + mt;
	}

}
