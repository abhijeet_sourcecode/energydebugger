package nus.edu.sg.ds;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Stack;

public class LogRecords {

	private List<MatchedString> records;
	private static Stack<Integer> stack;
	private int lastRecordOfSystemErr = -1;
	private int lastRecordOfThreadInit = -1;

	public LogRecords() {
		records = new ArrayList<MatchedString>();
	}

	public List<MatchedString> getRecords() {
		return records;
	}

	public boolean isLogEmpty() {
		return getRecords() == null ? true : false;
	}

	public void setRecords(List<MatchedString> rs) {
		records = rs;
	}

	public long getInstantTime(int i) {
		return getRecords().get(i).getInstantTime();
	}

	public int size() {
		return getRecords().size();
	}

	public String getMsg(int i) {
		return getRecords().get(i).getMsg();
	}

	public String getEventType(int i) {
		return getRecords().get(i).getEventType();
	}

	public int getMatchedIndex(int i) {
		return getRecords().get(i).getMatchedIndex();
	}

	public void addRecord(MatchedString mStr) {
		getRecords().add(mStr);
	}

	public boolean isResourceUsed(List<String> ps) {

		return isResourceUsed(ps, 0, size());
	}

	public MatchedString getRecordAt(int i) {
		return getRecords().get(i);
	}

	// check whether the given resource is used in log from start to end (not
	// including end)
	public boolean isResourceUsed(List<String> ps, int start, int end) {

		for (int i = start; i < end; i++) {
			final MatchedString s = getRecordAt(i);

			for (final String p : ps) {
				final int k = p.lastIndexOf(";->");
				String p1;
				if (k < 0) {
					p1 = p;
					if (s.getMsg().contains(p1)) {
						return true;
					}
				} else {
					p1 = p.substring(k + 3);
					final int k1 = p.lastIndexOf("\\");
					final String p2 = p.substring(k1 + 1, k);
					// this part needs to be changed
					if (s.getMsg().contains(p1 + "(") && s.getMsg().contains(p2)) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public boolean anyActivitySyscall(List<String> ps, int start, int end) {

		for (int i = start; i < end; i++) {
			final MatchedString s = getRecordAt(i);
			for (final String p : ps) {
				if (s.getMsg().contains(">" + p + "(") || s.getMsg().contains("_" + p + "(")) {
					return true;
				}
			}
		}

		return false;
	}

	public void readLogFromFile(String fullFilePath) {

		try {
			final BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fullFilePath)));
			stack = new Stack<Integer>();

			String line;
			while ((line = br.readLine()) != null) {

				/*
				 * if (line.contains("I/SELinux") || line.contains("I/dalvikvm")
				 * || line.contains("java/io/FileWriter") ||
				 * line.contains("java/io/PrintWriter") ||
				 * line.contains("I/System.out") ||
				 * line.contains("W/ApplicationPackageManager") ||
				 * line.contains("D/AbsListView(") ) continue;
				 */

				// if
				// (line.contains("09-16 15:25:00.391: I/EventHandler(14594): Finished_getHttpsUtil"))
				// {
				// System.out.println("");
				// }

				if (line.contains("I/EventHandler") || line.contains("V/DroidBox") || line.contains("W/System.err")
						|| line.contains("Exception")) {
					// || line.contains("(Unknown Source)")

					if (line.contains("W/System.err") || line.contains("Exception")) {
						lastRecordOfSystemErr = size();
						continue;
					}

					final MatchedString mStr = parseLine(line, size());

					if (mStr == null) {
						continue;
					}

					if (mStr.getMatchedIndex() >= 0) {
						getRecordAt(mStr.getMatchedIndex()).setMatchedIndex(size());
					}

					addRecord(mStr);
				}
			}
			br.close();

		} catch (final IOException e) {
			System.err.println("Error: " + fullFilePath + " does not exist!");
			System.exit(0);
		}

	}

	private MatchedString parseLine(String line, int logIndex) {

		final MatchedString mStr = new MatchedString();

		final String[] segments = line.split(" ");

		final Calendar time = Calendar.getInstance();

		// Month-Day format "MM-DD"
		time.set(Calendar.MONTH, Integer.valueOf(segments[0].substring(0, 2)) - 1); // set
		// Month
		time.set(Calendar.DATE, Integer.valueOf(segments[0].substring(3))); // set
		// Day

		// Time format HH:MM:SS.MSEC(3)
		time.set(Calendar.HOUR_OF_DAY, Integer.valueOf(segments[1].substring(0, 2))); // set
		// HOUR
		time.set(Calendar.MINUTE, Integer.valueOf(segments[1].substring(3, 5))); // set
		// MINUTE
		time.set(Calendar.SECOND, Integer.valueOf(segments[1].substring(6, 8))); // set
		// SECOND
		time.set(Calendar.MILLISECOND, Integer.valueOf(segments[1].substring(9, 12))); // set
		// MILLISECOND

		mStr.setInstantTime(time.getTimeInMillis());
		mStr.setEventType(segments[2]);

		// concatenate the rest of segments, from segment[3] to the end
		String restStr = "";
		int k = 3;
		while (k < segments.length) {
			restStr = restStr + " " + segments[k];
			k++;
		}

		String lMsg = restStr.trim();
		final int idx = lMsg.indexOf(":");
		if (idx != -1) {
			lMsg = lMsg.substring(idx + 1).trim();
		}
		mStr.setMsg(lMsg);
		mStr.getMsg();

		if (mStr.getEventType().contains("I/EventHandler")) {
			if (mStr.getMsg().startsWith("Finished_")) { // exit a method

				final String method = mStr.getMsg().substring(9);
				if (stack.isEmpty()) {
					System.out.printf("Warning: unmatched due to possible mulit-threading!\n");
					System.out.println(mStr.getMsg());
					return null;
				}
				int startIndex = stack.peek();
				boolean matched = true;

				if (!getRecords().get(startIndex).getMsg().contains(method)) {
					// unmatched situation

					// PRINCIPAL FOLLOWED: every finished method has a
					// corresponding start one,
					// but a start one may not have a finished corresponding one
					// due to (1) exception (2) multi-threading
					// at current version,

					/*
					 * // if following an exception, probably unmatching is
					 * caused by abnormal exit (exception) if
					 * (lastRecordOfSystemErr >= 0 && logIndex <=
					 * this.lastRecordOfSystemErr + 15) {
					 * getRecords().get(startIndex).setMatchedIndex(logIndex);
					 * stack.pop(); startIndex = stack.peek(); continue; }
					 */

					final int mIndex = findMethodInStack(method); /*
					 * mIndex is the
					 * record index
					 * matched with
					 * the current
					 * Finished
					 * method if
					 * mIndex >=0
					 */

					if (mIndex >= 0) { // locate a matched start method
						while (!stack.peek().equals(mIndex)) {
							stack.pop();
						}
						startIndex = mIndex;
					} else {
						matched = false;
					}
				}

				if (matched) {
					stack.pop();
					mStr.setMatchedIndex(startIndex);
				} else {
					return null;
				}

			} else { // start a method
				mStr.setMatchedIndex(-1);
				stack.push(logIndex);
			}
		} else {
			if (mStr.getMsg().contains("java/lang/Thread;-><init>")) {
				this.lastRecordOfThreadInit = logIndex;
			}

			mStr.setMatchedIndex(-2); // system event
		}

		return mStr;
	}

	private int findMethodInStack(String method) {

		int n = stack.size() - 1;

		while (n >= 0) {
			if (getRecords().get(stack.get(n)).getMsg().contains(method)) {
				break;
			} else {
				n--;
			}
		}

		if (n >= 0) {
			return stack.get(n);
		} else {
			return -1;
		}
	}

}
