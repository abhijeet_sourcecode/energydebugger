package nus.edu.sg.ds;

public class DNode {

	private DNode firstChild;
	private DNode nextSibling;
	private DNode parent;

	private String file;
	private String method;
	private int type; // dummy/system/event: 0/1/2
	private long runtime;
	private boolean resourceUsed;
	private long timeStamp;

	private int anyRuntimeException; // 0 - no / 1 - exception / 2 - system

	// error

	public DNode() {
		super();
		setFirstChild(null);
		setNextSibling(null);
		setParent(null);
		setFile(null);
		setMethod(null);
		setType(0);
		setRuntime(-1);
		setResourceUsed(false);
	}

	public DNode(DNode firstChild, DNode nextSibling, DNode parent, String file, String method, int type, int runtime,
			boolean resourceUsed) {
		super();
		setFirstChild(firstChild);
		setNextSibling(nextSibling);
		// System.out.println("dnode parent " + parent);
		setParent(parent);
		setFile(file);
		setMethod(method);
		setType(type);
		setRuntime(runtime);
		setResourceUsed(resourceUsed);
	}

	public String getClassName() {

		return this.getType() == 1 ? getSystemClassName() : getEventClassName();
		// 1 - system type 2 - event type
	}

	private String getEventClassName() {

		String className;

		className = getFile();
		if (className != null) {
			final int p = className.lastIndexOf("\\");
			className = className.substring(p + 1);
		} else {
			className = "";
		}

		return className;
	}

	private String getSystemClassName() {

		String className;

		className = getFile();
		if (className != null) {
			final int p = className.lastIndexOf("/");
			className = className.substring(p + 1);
		} else {
			className = "";
		}

		return className;
	}

	public String getMethodName() {

		String methodName;

		methodName = getMethod();
		if (methodName != null) {
			final int p = methodName.indexOf("(");
			methodName = methodName.substring(0, p);
		} else {
			methodName = "";
		}

		return methodName;
	}

	@Override
	public String toString() {
		return String.format("\"{{%s | %d }|{%s | %s} }\"", getClassName(), getRuntime(), getMethodName(),
				isResourceUsed());
	}

	public int getAnyRuntimeException() {
		return anyRuntimeException;
	}

	public void setAnyRuntimeException(int anyRuntimeException) {
		this.anyRuntimeException = anyRuntimeException;
	}

	public DNode getFirstChild() {
		return firstChild;
	}

	public void setFirstChild(DNode firstChild) {
		this.firstChild = firstChild;
	}

	public DNode getNextSibling() {
		return nextSibling;
	}

	public void setNextSibling(DNode nextSibling) {
		this.nextSibling = nextSibling;
	}

	public DNode getParent() {
		return parent;
	}

	public void setParent(DNode parent) {
		this.parent = parent;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public long getRuntime() {
		return runtime;
	}

	public void setRuntime(long runtime) {
		this.runtime = runtime;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public boolean isResourceUsed() {
		return resourceUsed;
	}

	public void setResourceUsed(boolean resourceUsed) {
		this.resourceUsed = resourceUsed;
	}

}