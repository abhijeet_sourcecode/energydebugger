package nus.edu.sg.ds;

public class EnergyBug extends Defect {

	public EnergyBug(String cn, String mn) {
		this.type = Defect.BUG;
		setFile(cn);
		setMethod(mn);
	}

}
