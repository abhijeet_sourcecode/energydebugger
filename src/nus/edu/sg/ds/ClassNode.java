package nus.edu.sg.ds;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;

public class ClassNode {

	String fileName;
	String name;
	String packageName;
	List<MethodNode> mList;
	IFile ifile;
	List<LoopNode> loops;
	boolean isActivity = false;
	boolean isService = false;
	boolean isListener = false;

	public ClassNode(String fileName, String name, String packageName, IFile ifile) {
		super();
		this.fileName = fileName;
		this.name = name;
		this.packageName = packageName;
		this.mList = new ArrayList<MethodNode>();
		this.loops = new ArrayList<LoopNode>();
		this.ifile = ifile;
	}

	public boolean isActivity() {
		return isActivity;
	}

	public boolean isService() {
		return isService;
	}

	public boolean isListener() {
		return isListener;
	}

	public void setActivity() {
		isActivity = true;
	}

	public void setService() {
		isService = true;
	}

	public List<LoopNode> getLoops() {
		return this.loops;
	}

	public void addLoops(LoopNode loop) {
		this.loops.add(loop);
	}

	public void addMethod(MethodNode mNode) {
		this.mList.add(mNode);

	}

	@Override
	public String toString() {
		String methods = "";

		for (final MethodNode m : mList) {
			methods += m.toString() + ",";
		}
		return packageName + ":" + name + "[" + methods + "]";
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public List<MethodNode> getmList() {
		return mList;
	}

	public void setmList(List<MethodNode> mList) {
		this.mList = mList;
	}

	public IFile getIfile() {
		return ifile;
	}

	public void setIfile(IFile ifile) {
		this.ifile = ifile;
	}

	public void setListener() {
		this.isListener = true;

	}
}
