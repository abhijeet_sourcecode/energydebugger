package nus.edu.sg.ds;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class GuidelineData.
 */
public class GuidelineData {

	/** The id. */
	public int id;

	/** The name. */
	public String name;

	/** The type. */
	public String type;

	/** The is pair. */
	public boolean isPair;

	/** The is use. */
	public boolean isUse;

	/** The syscall1. */
	public String syscall1;

	/** The syscall2. */
	public String syscall2;
	
	public List<String> useCalls;

	/** The is ref counted. */
	public boolean isRefCounted;

	/** The sys call ref. */
	public String sysCallRef;

	/** The arguments. */
	public List<String> arguments;

	/**
	 * Instantiates a new guideline data.
	 *
	 * @param id
	 *            the id
	 * @param name
	 *            the name
	 * @param type
	 *            the type
	 * @param isPair
	 *            the is pair
	 * @param isUse
	 *            the is use
	 * @param syscall1
	 *            the syscall1
	 * @param syscall2
	 *            the syscall2
	 * @param isRefCounted
	 *            the is ref counted
	 * @param sysCallRef
	 *            the sys call ref
	 * @param arguments
	 *            the arguments
	 */
	public GuidelineData(int id, String name, String type, boolean isPair, boolean isUse, String syscall1,
			String syscall2, boolean isRefCounted, String sysCallRef, List<String> arguments) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.isPair = isPair;
		this.isUse = isUse;
		this.syscall1 = syscall1;
		this.syscall2 = syscall2;
		this.isRefCounted = isRefCounted;
		this.sysCallRef = sysCallRef;
		this.arguments = arguments;
	}

	/**
	 * Instantiates a new guideline data.
	 */
	public GuidelineData() {
		super();
		this.id = -1;
		this.name = null;
		this.type = null;
		this.isPair = false;
		this.isUse = false;
		this.syscall1 = null;
		this.syscall2 = null;
		this.isRefCounted = false;
		this.sysCallRef = null;
		this.arguments = null;
	}
	
	
	public void setUseCalls(List<String> calls) {
		useCalls = calls;
	}
	
	public List<String> getUseCalls() {
		return useCalls;
	}
	

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Checks if is pair.
	 *
	 * @return true, if is pair
	 */
	public boolean isPair() {
		return isPair;
	}

	/**
	 * Sets the pair.
	 *
	 * @param isPair
	 *            the new pair
	 */
	public void setPair(boolean isPair) {
		this.isPair = isPair;
	}

	/**
	 * Checks if is use.
	 *
	 * @return true, if is use
	 */
	public boolean isUse() {
		return isUse;
	}

	/**
	 * Sets the use.
	 *
	 * @param isUse
	 *            the new use
	 */
	public void setUse(boolean isUse) {
		this.isUse = isUse;
	}

	/**
	 * Gets the syscall1.
	 *
	 * @return the syscall1
	 */
	public String getSyscall1() {
		return syscall1;
	}

	/**
	 * Sets the syscall1.
	 *
	 * @param syscall1
	 *            the new syscall1
	 */
	public void setSyscall1(String syscall1) {
		this.syscall1 = syscall1;
	}

	/**
	 * Gets the syscall2.
	 *
	 * @return the syscall2
	 */
	public String getSyscall2() {
		return syscall2;
	}

	/**
	 * Sets the syscall2.
	 *
	 * @param syscall2
	 *            the new syscall2
	 */
	public void setSyscall2(String syscall2) {
		this.syscall2 = syscall2;
	}

	/**
	 * Checks if is ref counted.
	 *
	 * @return true, if is ref counted
	 */
	public boolean isRefCounted() {
		return isRefCounted;
	}

	/**
	 * Sets the ref counted.
	 *
	 * @param isRefCounted
	 *            the new ref counted
	 */
	public void setRefCounted(boolean isRefCounted) {
		this.isRefCounted = isRefCounted;
	}

	/**
	 * Gets the sys call ref.
	 *
	 * @return the sys call ref
	 */
	public String getSysCallRef() {
		return sysCallRef;
	}

	/**
	 * Sets the sys call ref.
	 *
	 * @param sysCallRef
	 *            the new sys call ref
	 */
	public void setSysCallRef(String sysCallRef) {
		this.sysCallRef = sysCallRef;
	}

	/**
	 * Gets the arguments.
	 *
	 * @return the arguments
	 */
	public List<String> getArguments() {
		return arguments;
	}

	/**
	 * Sets the arguments.
	 *
	 * @param arguments
	 *            the new arguments
	 */
	public void setArguments(List<String> arguments) {
		this.arguments = arguments;
	}

}