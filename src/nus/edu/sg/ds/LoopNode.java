package nus.edu.sg.ds;

public class LoopNode {
	public enum LoopType {
		DO, WHILE, FOR
	};

	LoopType lType;
	String body; // can ommit if takes too much space
	int startLine;
	int endLine;

	public LoopType getlType() {
		return lType;
	}

	public void setlType(LoopType lType) {
		this.lType = lType;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public int getStartLine() {
		return startLine;
	}

	public void setStartLine(int startLine) {
		this.startLine = startLine;
	}

	public int getEndLine() {
		return endLine;
	}

	public void setEndLine(int endLine) {
		this.endLine = endLine;
	}

	public LoopNode(LoopType lType, String body, int startLine, int endLine) {
		super();
		this.lType = lType;
		this.body = body;
		this.startLine = startLine;
		this.endLine = endLine;
	}
}
