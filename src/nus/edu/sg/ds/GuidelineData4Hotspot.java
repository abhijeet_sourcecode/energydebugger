package nus.edu.sg.ds;
import java.util.Hashtable;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class GuidelineData.
 */
public class GuidelineData4Hotspot {

	/** The id. */
	private int id;

	/** The name. */
	private String name;

	/** The type. */
	private String type;

	/** The is exclusive. */
	private boolean exclusive;

	/** The begincall. */
	private String begincall;

	/** The usage syscalls */
	private List<String> usagecalls;
	private Hashtable<String, String> pairedUseCalls;

	/** The endcall. */
	private String endcall;


	public GuidelineData4Hotspot(int id, String name, String type, boolean exclusive, String begincall,
			List<String> usagecalls, Hashtable<String, String> pcalls, String endcall) {
		super();
		setId(id);
		setName(name);
		setType(type);
		setExclusive(exclusive);
		setBegincall(begincall);
		setUsagecalls(usagecalls);
		setPairedUseCalls(pcalls);
		setEndcall(endcall);
	}

	public GuidelineData4Hotspot() {
		super();
		setId(-1);
		setName(null);
		setType(null);
		setExclusive(false);
		setBegincall(null);
		setUsagecalls(null);
		setPairedUseCalls(null);
		setEndcall(null);
	}


	public Hashtable<String, String> getPairedUseCalls() {
		return pairedUseCalls;
	}

	public void setPairedUseCalls(Hashtable<String, String> pairCalls) {
		this.pairedUseCalls = pairCalls;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public boolean isExclusive() {
		return exclusive;
	}


	public void setExclusive(boolean exclusive) {
		this.exclusive = exclusive;
	}


	public String getBegincall() {
		return begincall;
	}


	public void setBegincall(String begincall) {
		this.begincall = begincall;
	}


	public List<String> getUsagecalls() {
		return usagecalls;
	}


	public void setUsagecalls(List<String> usagecalls) {
		this.usagecalls = usagecalls;
	}


	public String getEndcall() {
		return endcall;
	}


	public void setEndcall(String endcall) {
		this.endcall = endcall;
	}

	

}