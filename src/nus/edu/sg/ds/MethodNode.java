package nus.edu.sg.ds;

import java.util.List;

public class MethodNode {

	String name;
	String type;
	int lineNumber;
	List<String> parameters;
	List<MethodNode> internalMthds;
	int endLineNumber;

	public MethodNode(String name, List<String> parameters, String type, int lineNumber, int endLineNumber,
			List<MethodNode> intenalMthds) {
		super();
		this.name = name;
		this.type = type;
		this.parameters = parameters;
		this.lineNumber = lineNumber;
		this.endLineNumber = endLineNumber;
		this.internalMthds = intenalMthds;
	}

	@Override
	public String toString() {
		return name + "-" + lineNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public List<String> getParameters() {
		return parameters;
	}

	public void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}

	public List<MethodNode> getInternalMthds() {
		return internalMthds;
	}

	public void setInternalMthds(List<MethodNode> internalMthds) {
		this.internalMthds = internalMthds;
	}

	public int getEndLineNumber() {
		return endLineNumber;
	}

	public void setEndLineNumber(int endLineNumber) {
		this.endLineNumber = endLineNumber;
	}

}
