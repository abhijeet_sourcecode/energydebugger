package nus.edu.sg.ds;


public class Hotspot extends Defect {

	private long runtime;
	public float frequency;
	public String params;

	public Hotspot(String cn, String mn, long t) {
		this.type = 2;
		setFile(cn);
		setMethod(mn);
		setRuntime(t);
		// setFrequency(f);
	}

	public long getRuntime() {
		return runtime;
	}

	public void setRuntime(long time) {
		this.runtime = time;
	}

}
