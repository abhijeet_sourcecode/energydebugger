package nus.edu.sg.ds;

import java.util.List;

public class DefectInfo {

	private List<Defect> defects;
	private List<EnergyAux> activityEnv;
	private List<EnergyAux> resourceUse;

	public DefectInfo(List<Defect> defects, List<EnergyAux> activityEnv, List<EnergyAux> resourceUse) {
		super();
		this.defects = defects;
		this.activityEnv = activityEnv;
		this.resourceUse = resourceUse;
	}

	public List<Defect> getDefect() {
		return defects;
	}

	public void setDefect(List<Defect> defects) {
		this.defects = defects;
	}

	public List<EnergyAux> getActivityEnv() {
		return activityEnv;
	}

	public void setActivityEnv(List<EnergyAux> activityEnv) {
		this.activityEnv = activityEnv;
	}

	public List<EnergyAux> getResourceUse() {
		return resourceUse;
	}

	public void setResourceUse(List<EnergyAux> resourceUse) {
		this.resourceUse = resourceUse;
	}

	@Override
	public String toString() {

		if (defects.size() > 1) {
			System.out.println("MOre than one defect, add code");
			System.out.println(defects);
			// System.exit(0);
		}

		return defects.get(0).toString();

	}

	public void fullInfo() {
		for (final EnergyAux a : activityEnv) {
			System.out.println("energyaux " + a.toString());
		}

	}
}
