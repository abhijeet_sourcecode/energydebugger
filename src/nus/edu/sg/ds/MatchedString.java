package nus.edu.sg.ds;

public class MatchedString {
	private long instantTimeInMille;
	private String eventType;
	private String msg;
	private int matchedIndex;   // set -1 if it itself is an open event and there is a matched close event
	                                // set -2 if it is a system alone
	
	
	public long getInstantTime() {
		return instantTimeInMille;
	}
	public void setInstantTime(long instantTime) {
		this.instantTimeInMille = instantTime;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getMatchedIndex() {
		return matchedIndex;
	}
	public void setMatchedIndex(int matchedIndex) {
		this.matchedIndex = matchedIndex;
	}
	
}


