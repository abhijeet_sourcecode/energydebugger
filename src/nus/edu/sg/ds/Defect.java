package nus.edu.sg.ds;

import java.util.List;

import nus.edu.sg.Global;

public class Defect {
	public static final int BUG = 1;
	public static final int HOTSPOT = 2;

	public int type; // bug or hotspot
	public int subType; // bug or hotspot category
	public String file; // the full path/class where the method <M> is defined
	public String method; // the method name <M>
	public String caller; // the full path/class and method name of the caller
	// of <M>, in form of <class>;-><method>
	public List<String> next5SiblingMethods; // the next 5 called events or
	public int debugPriority = 0;
	private long timeStamp;
	public List<DNode> before = null;
	public List<DNode> after = null;
	public String patchString = null;
	public List<EnergyAux> envn;

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {

		String temp = caller;
		temp = temp.replace("/", ".").replace("\\", ".");
		if (temp.contains(Global.instrumentationDir)) {
			// temp = temp.substring(temp.indexOf(Global.instrumentationDir) +
			// Global.instrumentationDir.length());
			temp = Global.getWithoutDir(temp);
		}

		this.caller = temp;
	}

	public void setSubType(int t) {
		subType = t;
	}

	public int getSubType() {
		return subType;
	}

	public List<String> getNext5SiblingMethods() {
		return next5SiblingMethods;
	}

	public void setNext5SiblingMethods(List<String> ms) {
		this.next5SiblingMethods = ms;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Override
	public String toString() {
		final String fn = file;
		final String mt = method;
		final String cl = caller;
		return cl + ":" + mt;
	}

	public void fullInfo() {
		System.out.println("file " + file);
		System.out.println("method " + method);
		System.out.println("caller " + caller);
		System.out.println("sibling " + next5SiblingMethods);
	}
}
