package nus.edu.sg.ds;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.Hashtable;
import java.util.List;

import nus.edu.sg.Global;
import nus.edu.sg.utility.Utility;

public class DTree {

	private final static String TAB = "    ";
	public final static int SYSTEMNode = 1;
	public final static int EVENTNode = 2;
	private Formatter out;
	private int nodeIndex = 0;

	private DNode root;
	private final List<DNode> sysUseNodes; // nodes related to resource usage
	private List<DNode> activityNodes; // nodes related to activity life-cycle

	public DTree() {
		super();
		setRoot(newDummyNode());
		sysUseNodes = new ArrayList<DNode>();
		activityNodes = new ArrayList<DNode>();
	}

	public void buildDTree(LogRecords logRecords, List<String> filterPatterns) {

		final DNode thisNode = getRoot();

		final long starttime = logRecords.getInstantTime(0);
		final int end = logRecords.size() - 1;
		final long endtime = logRecords.getInstantTime(end);
		final long runtime = endtime - starttime;
		thisNode.setRuntime(runtime);
		thisNode.setTimeStamp(starttime);
		thisNode.setFile("AndroidApp");
		final boolean debugFlag = logRecords.isResourceUsed(filterPatterns, 0, end);
		thisNode.setResourceUsed(debugFlag ? true : false);

		readTillFinished(0, end + 1, thisNode, logRecords, filterPatterns); // 0
		// is
		// the
		// initial
		// index
		// of
		// the
		// log
		// file
		// the unreachable -1 denotes the start index for the dummy node,
		// so, it will read till the end of the log file
	}

	/*
	 * start is the log index for starting a method <M> the procedure
	 * readTillFinished will read logs till the exit of the method <M>, and
	 * build a subtree rooted at pNode
	 */
	private void readTillFinished(int start, int xend, DNode pNode, LogRecords records, List<String> filterPatterns) {

		long starttime, endtime, runtime;
		DNode q = null;
		final List<String> activityPatterns = initActivityPatterns();

		// for (int i = 0; i < records.size(); i++) {
		// System.out.println(records.getRecordAt(i).getMsg());
		// }

		while (start < xend) {

			if (records.getRecordAt(start).getEventType().contains("EventHandler")) { // method
				// entry
				// if
				// (records.getRecordAt(start).getMsg().contains("EventManager_registerListener"))
				// System.out.println();

				starttime = records.getInstantTime(start);
				final int end = records.getMatchedIndex(start);
				// System.out.println("==> " +
				// records.getRecordAt(start).getMsg());
				if (end == -1) {
					start++;

					continue;
				}
				endtime = records.getInstantTime(end);
				runtime = endtime - starttime;

				final boolean debugFlag = records.isResourceUsed(filterPatterns, start, end);
				final boolean activityFlag = records.anyActivitySyscall(activityPatterns, start, end);

				// skip if the method doesn't use thisResource and zero runtime
				if (!debugFlag && runtime == 0 && !activityFlag) {
					// if (!debugFlag && runtime == 0) {
					start = end + 1;
					continue;
				}

				final MatchedString mStr = records.getRecordAt(start);
				final String tempStr = mStr.getMsg();
				int u = tempStr.lastIndexOf("_");
				if (u < 0) {
					u = tempStr.length();
				}

				if (q == null) { // the first child
					q = new DNode();
					pNode.setFirstChild(q);
				} else {
					final String fileName = tempStr.substring(0, u);
					final String methodName = tempStr.substring(u + 1);

					// if (fileName.equals(q.getFile()) &&
					// methodName.equals(q.getMethod()) && !debugFlag) {
					if (fileName.equals(q.getFile()) && methodName.equals(q.getMethod())) {
						// combine sibling nodes since same
						q.setRuntime(q.getRuntime() + runtime);
						// start = end + 1;
						// disabled for debug
						// continue;
					} else {
						q.setNextSibling(new DNode());
						q = q.getNextSibling();
					}
				}

				q.setParent(pNode);

				q.setFile(tempStr.substring(0, u));
				q.setMethod(tempStr.substring(u + 1));
				q.setType(DTree.EVENTNode); // 1 - system; 2 - event
				q.setRuntime(runtime);
				q.setTimeStamp(starttime);
				// System.out.println("readtillfinished parent " + q.getFile() +
				// " : " + q.getMethod());

				if (debugFlag) {
					q.setResourceUsed(true);

					// if (records.isResourceUsed(filterPatterns, start, start +
					// 1)) {
					if (records.isResourceUsed(filterPatterns, start, end)) {
						getSysUseNodes().add(q); // add into the sysUseList
					}

				} else {
					q.setResourceUsed(false);
				}

				if (records.getEventType(end).contains("W/")) {
					q.setAnyRuntimeException(2); // system error
				} else if (records.getEventType(end).contains("E/")) {
					q.setAnyRuntimeException(1); // system exception
				} else {
					q.setAnyRuntimeException(0); // no error or exception
				}

				if (isEventActivityPattern(tempStr.substring(tempStr.lastIndexOf("\\")), activityPatterns)) { // if
					// the
					// node
					// q
					// is
					// an
					// activity
					// node
					if (!this.getActivityNodes().contains(q)) {
						this.getActivityNodes().add(q); // add into the
						// activityNodes List
					}
				}

				if (debugFlag || activityFlag) {
					// if (debugFlag) {
					readTillFinished(start + 1, end, q, records, filterPatterns);
				}

				start = end + 1;
			} else { // system event

				if (start < records.size() - 1) {
					runtime = records.getInstantTime(start + 1) - records.getInstantTime(start);
				} else {
					runtime = 0;
				}

				final boolean debugFlag = records.isResourceUsed(filterPatterns, start, start + 1);
				final boolean isActivityRecord = isSysActivityPattern(records.getMsg(start), activityPatterns);

				// skip if the systemcall is irrelevant to thisResource
				if (!debugFlag && runtime == 0 && !isActivityRecord) {
					start++;
					continue;
				}

				final String[] cm = records.getMsg(start).split(";->");
				String methodName;
				if (cm.length <= 1) {
					methodName = "";
				} else {
					methodName = cm[1];
				}

				if (q == null) { // the first child
					q = new DNode();
					pNode.setFirstChild(q);
				} else if (cm[0].equals(q.getFile()) && methodName.equals(q.getMethod())) {
					// combine sibling nodes since same
					q.setRuntime(q.getRuntime() + runtime);
					start++;
					continue;
				} else {
					q.setNextSibling(new DNode());
					q = q.getNextSibling();
				}

				q.setParent(pNode);

				q.setFile(cm[0]);
				q.setMethod(methodName);
				q.setType(DTree.SYSTEMNode); // 1 - system; 2 - event

				q.setRuntime(runtime);
				q.setTimeStamp(records.getInstantTime(start));

				if (debugFlag) {
					q.setResourceUsed(true);
					getSysUseNodes().add(q); // add into the sysUseList in the
					// tree
				} else if (isActivityRecord) {
					q.setResourceUsed(false);
					this.getActivityNodes().add(q); // add into the
					// activityNodes List

				} else {
					q.setResourceUsed(false);
				}

				start++;
			}
		}
	}

	private boolean isEventActivityPattern(String msg, List<String> activityPatterns) {

		for (final String p : activityPatterns) {
			// if (msg.contains("Activity_"+p+"(")) {
			if (p.contains("onCreate")) {
				if (msg.contains(p)) {
					return true;
				}
			} else if (msg.contains("_" + p + "(")) {
				return true;
			}
		}

		return false;
	}

	private boolean isSysActivityPattern(String msg, List<String> activityPatterns) {

		for (final String p : activityPatterns) {
			if (msg.contains("android/app/Activity;->" + p + "(")) {
				return true;
			}
		}

		return false;
	}

	private List<String> initActivityPatterns() {

		final List<String> ps = new ArrayList<String>();

		ps.add("onCreate(Landroid/os/Bundle;)");
		ps.add("onRestart");
		ps.add("onResume");
		ps.add("onPause");
		ps.add("onStop");
		ps.add("onDestroy");

		/*
		 * ps.add("android/app/Activity;->onCreate");
		 * ps.add("android/app/Activity;->onRestart");
		 * ps.add("android/app/Activity;->onResume");
		 * ps.add("android/app/Activity;->onPause");
		 * ps.add("android/app/Activity;->onStop");
		 * ps.add("android/app/Activity;->onDestroy");
		 */
		return ps;

	}

	public void tree2dot(String file) {

		try {
			out = new Formatter(new File(file));

			out.format("digraph ProfileGraph {\n");
			// out.format(TAB+"size = \"7.5,10.5\";\n");
			// out.format(TAB+"node [shape = record, height = .2];\n\n");

			out.format("%s%s;\n", TAB, recordNode2str(nodeIndex++, root));
			subtree2dot(root);

			out.format("}");
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
			out = null;
		}

		out.close();
	}

	// i is the nodeIndex used in the output dot file to differentiate nodes
	private void subtree2dot(DNode pNode) {

		DNode thisChild = pNode.getFirstChild();
		int thisIndex = 0;
		final int pIndex = getNodeIndex() - 1;
		final List<String> sameRankList = new ArrayList<String>();

		// add a directed edge from the (parent) pNode to the (first child)
		// thisChild if exists
		if (thisChild != null) {
			out.format("%s%s;\n", TAB, edge2str(pIndex, getNodeIndex())); // i-1
			// and
			// i
			// are
			// the
			// node
			// indexes
			// for
			// pNode
			// and
			// thisChild,
			// respectively
		}

		while (thisChild != null) {

			thisIndex = getNodeIndex();
			sameRankList.add("node" + thisIndex);

			if (thisChild.getType() == SYSTEMNode && thisChild.isResourceUsed()) {
				out.format("%s%s;\n", TAB, mrecordNode2str(thisIndex, thisChild));
				incNodeIndex();
			} else {
				out.format("%s%s;\n", TAB, recordNode2str(thisIndex, thisChild));
				incNodeIndex();
				subtree2dot(thisChild);
			}

			if (thisChild.getNextSibling() != null) {
				out.format("%s%s;\n", TAB, edge2str(thisIndex, getNodeIndex())); // thisIndex
				// and
				// i
				// are
				// the
				// node
				// indexes
				// for
				// thisNode
				// and
				// its
				// next
				// sibling,
				// respectively
			} else { // for the last sibling, add an edge from the (parent node)
				// pNode to this (child node) thisChild if thisChild is
				// not the firstChild.
				if (thisChild != pNode.getFirstChild()) {
					out.format("%s%s;\n", TAB, edge2str(pIndex, thisIndex)); // thisIndex
					// and
					// i
					// are
					// the
					// node
					// indexes
					// for
					// thisNode
					// and
					// its
					// next
					// sibling,
					// respectively
				}
			}

			thisChild = thisChild.getNextSibling();

		}

		String sameRank = "{ rank = same; ";
		for (final String item : sameRankList) {
			sameRank = sameRank + item + ";";
		}
		sameRank += "}\n";
		out.format(sameRank);
	}

	private String edge2str(int iP, int iC) {

		return String.format("node%d -> node%d", iP, iC);

	}

	private String recordNode2str(int i, DNode n) {

		return String.format("node%d [shape = record, label = %s]", i, n);
	}

	private String mrecordNode2str(int i, DNode n) {

		return String.format("node%d [shape = Mrecord, label = %s]", i, n);
	}

	private int incNodeIndex() {
		setNodeIndex(getNodeIndex() + 1);
		return getNodeIndex();
	}

	public DNode getRoot() {
		return root;
	}

	public void setRoot(DNode root) {
		this.root = root;
	}

	public int getNodeIndex() {
		return nodeIndex;
	}

	public void setNodeIndex(int nodeIndex) {
		this.nodeIndex = nodeIndex;
	}

	public List<DNode> getSysUseNodes() {
		return sysUseNodes;
	}

	public List<DNode> getActivityNodes() {
		return activityNodes;
	}

	public void setActivityNodes(List<DNode> nodes) {
		activityNodes = nodes;
	}

	private DNode newDummyNode() {
		return new DNode();
	}

	public static void setFirstChild(DNode pNode, DNode cNode) {
		pNode.setFirstChild(cNode);
		cNode.setParent(pNode);
		// System.out.println("setfirst child parent " + pNode);
	}

	public static void setNextSibling(DNode node, DNode nextNode) {
		node.setNextSibling(nextNode);
		nextNode.setParent(node.getParent());
		// System.out.println("setNextSibling child parent " +
		// node.getParent());

	}

	public void addSysUseNode(DNode node) {
		getSysUseNodes().add(node);
	}

	public void hotspotTraversal(int from, int to, List<Hotspot> hotspotTable) {
		final DNode thisNode = getResUseNode(from);
		final DNode nextNode = getResUseNode(to);
		seekNextSiblingHotspot(thisNode, nextNode, hotspotTable);
	}

	private void seekNextSiblingHotspot(DNode thisNode, DNode nextNode, List<Hotspot> hotspotTable) {

		final DNode p = thisNode.getNextSibling();

		if (p == null) {
			seekParentHotspot(thisNode, nextNode, hotspotTable);
		} else { // if there is a sibling
			if (p == nextNode) {
				return;
			}

			if (p.getType() == SYSTEMNode) {
				if (p.getRuntime() > Global.configurableRuntine) {
					addIntoTable(hotspotTable, p);
				}
				seekNextSiblingHotspot(p, nextNode, hotspotTable);
			} else { // event node
				if (p.isResourceUsed()) {
					seeFirstchildHotspot(p, nextNode, hotspotTable);
				} else {
					if (p.getRuntime() >= 45) { // set the hotspot time
						// threshold at 45ms
						addIntoTable(hotspotTable, p);
					}
					seekNextSiblingHotspot(p, nextNode, hotspotTable);
				}
			}
		}

	}

	private void seeFirstchildHotspot(DNode thisNode, DNode nextNode, List<Hotspot> hotspotTable) {

		final DNode p = thisNode.getFirstChild();

		if (p == null) {
			seekNextSiblingHotspot(thisNode, nextNode, hotspotTable);
		} else { // if there is a sibling
			if (p == nextNode) {
				return;
			}

			if (p.getType() == SYSTEMNode) {
				if (p.getRuntime() > 0) {
					addIntoTable(hotspotTable, p);
				}
				seekNextSiblingHotspot(p, nextNode, hotspotTable);
			} else { // event node
				if (p.isResourceUsed()) {
					seeFirstchildHotspot(p, nextNode, hotspotTable);
				} else {
					addIntoTable(hotspotTable, p);
					seekNextSiblingHotspot(p, nextNode, hotspotTable);
				}
			}
		}

	}

	public DNode getResUseNode(int i) {

		return getSysUseNodes().get(i);
	}

	private void addIntoTable(List<Hotspot> hotspotTable, DNode p) {

		final Hotspot spot = new Hotspot(p.getFile(), p.getMethod(), p.getRuntime());
		final DNode caller = p.getParent();

		if (caller.getMethod() == null) {
			spot.setCaller(p.getFile() + ";->" + p.getMethod());
		} else {
			spot.setCaller(caller.getFile() + ";->" + caller.getMethod());
		}

		final List<String> ms = new ArrayList<String>();

		int count = 5; // 5 next sibling call, if any
		while (p.getNextSibling() != null && count > 0) {
			p = p.getNextSibling();
			ms.add(p.getFile() + ";->" + p.getMethod());
			count--;
		}

		spot.setTimeStamp(p.getTimeStamp());
		spot.setNext5SiblingMethods(ms);
		hotspotTable.add(spot);
	}

	private void seekParentHotspot(DNode thisNode, DNode nextNode, List<Hotspot> hotspotTable) {

		if (thisNode.getParent() == null) {
			return;
		}

		seekNextSiblingHotspot(thisNode.getParent(), nextNode, hotspotTable);
	}

	public int findPairedStopNode(int i, GuidelineData4Hotspot thisResource) {

		final DNode thisNode = getSysUseNodes().get(i);
		final String cname = thisNode.getClassName();
		final String mname = thisNode.getMethodName();
		String stopPattern = null;

		final Hashtable<String, String> pairs = thisResource.getPairedUseCalls();

		for (final String key : pairs.keySet()) {

			if (key.contains(cname) && key.contains(mname)) {
				stopPattern = pairs.get(key);
				break;
			}
		}

		if (stopPattern == null) {
			return -1;
		}

		final List<DNode> resNodes = getSysUseNodes();

		i++;
		while (i < resNodes.size()) {
			if (Utility.inPattern(resNodes.get(i), stopPattern)) {
				return i;
			} else {
				i++;
			}
		}

		return -1;
	}

	public int findNextNodeInPattern(String p) {
		return findNextNodeInPattern(-1, p);
	}

	public int findNextNodeInPattern(List<String> p) {
		return findNextNodeInPattern(-1, p);
	}

	public int findNextNodeInPattern(int i, String p) {

		final List<DNode> resNodes = getSysUseNodes();

		i++;
		while (i < resNodes.size()) {
			if (Utility.inPattern(resNodes.get(i), p)) {
				return i;
			} else {
				i++;
			}
		}

		return i;
	}

	public int findNextNodeInPattern(int i, List<String> ps) {

		final List<DNode> resNodes = getSysUseNodes();

		if (ps == null) {
			return resNodes.size();
		}

		i++;
		while (i < resNodes.size()) {
			if (Utility.inPattern(resNodes.get(i), ps)) {
				return i;
			} else {
				i++;
			}
		}

		return -1;
	}

	// begin is an index in the list of resource usage, typically when the
	// resource is acquired
	// end is the next onResume in the list of activity lifecycle
	// we use timestamp to coordinate the partial order of these two lists
	// (resource usages and activity lifecycle)
	public int findActivityOnPauseBetween(int begin, int end) {

		if (begin == getActivityNodes().size()) {
			return begin;
		}

		if (begin < 0) {
			begin = 0;
		}

		final long acquireTimeStamp = getActivityNodes().get(begin).getTimeStamp();

		int i = end - 1;
		int indexOnPause = getActivityNodes().size();

		while (i > begin && getActivityNodes().get(i).getTimeStamp() > acquireTimeStamp) {

			if (getActivityNodes().get(i).getMethodName().contains("onPause")) {
				indexOnPause = i;
			}

			i--;
		}

		return indexOnPause;
	}

	// acquireNodeIndex is an index in the list of resource usage
	public int findActivityOnResumeAfterAcquire(int acquireNodeIndex) {

		int activityIndex = getActivityNodes().size();
		final long acquireTimeStamp = getSysUseNodes().get(acquireNodeIndex).getTimeStamp();

		int i = getActivityNodes().size() - 1;

		while (i >= 0 && getActivityNodes().get(i).getTimeStamp() > acquireTimeStamp) {
			if (getActivityNodes().get(i).getMethodName().contains("onResume")
					|| getActivityNodes().get(i).getMethodName().contains("onCreate")) {

				activityIndex = i;
			}

			i--;
		}

		return activityIndex;

	}

	public int findActivityOnResumeAfterActivity(int activityIndex) {

		int i = activityIndex + 1;

		while (i < getActivityNodes().size()) {
			if (getActivityNodes().get(i).getMethodName().contains("onResume")
					|| getActivityNodes().get(i).getMethodName().contains("onCreate")) {
				return i;
			}
			i++;
		}

		return i; // same as getActivityNodes().size()
	}

	// acquireNodeIndex is an index in the list of resource usage

	public int findActivityOnStopAfterAcquire(int acquireNodeIndex) {

		int activityIndex = getActivityNodes().size();
		final long acquireTimeStamp = getSysUseNodes().get(acquireNodeIndex).getTimeStamp();

		int i = getActivityNodes().size() - 1;

		while (i >= 0 && getActivityNodes().get(i).getTimeStamp() > acquireTimeStamp) {
			if (getActivityNodes().get(i).getMethodName().contains("onStop")) {
				activityIndex = i;
			}

			i--;
		}

		return activityIndex;

	}

	// acquireNodeIndex is an index in the list of resource usage
	public int findActivityOnCreateBeforeAcquire(int acquireNodeIndex) {

		int activityIndex = -1;
		final long acquireTimeStamp = getSysUseNodes().get(acquireNodeIndex).getTimeStamp();

		for (int i = 0; i < getActivityNodes().size(); i++) {

			if (getActivityNodes().get(i).getTimeStamp() < acquireTimeStamp) {
				if (getActivityNodes().get(i).getMethodName().contains("onCreate")) {
					activityIndex = i;
				}
			} else {
				break;
			}
		}

		return activityIndex;
	}

	// acquireNodeIndex is an index in the list of resource usage
	public int findActivityOnResumeBeforeAcquire(int acquireNodeIndex) {

		int activityIndex = -1;
		final long acquireTimeStamp = getSysUseNodes().get(acquireNodeIndex).getTimeStamp();

		for (int i = 0; i < getActivityNodes().size(); i++) {

			if (getActivityNodes().get(i).getTimeStamp() < acquireTimeStamp) {

				if (getActivityNodes().get(i).getMethodName().contains("onResume")) {
					// ||
					// getActivityNodes().get(i).getMethodName().contains("onCreate")
					// ) {
					activityIndex = i;
				}
			} else {
				break;
			}
		}

		return activityIndex;
	}

	// begin is an index in the list of resource usage, typically when the
	// resource is acquired
	// end is the next onResume in the list of activity lifecycle
	// we use timestamp to coordinate the partial order of these two lists
	// (resource usages and activity lifecycle)
	public int findUnregisterPatternBetween(int begin, int end, String releasePattern) {

		final long endTimeStamp;

		if (end >= getActivityNodes().size()) {
			endTimeStamp = Long.MAX_VALUE;
		} else {
			endTimeStamp = getActivityNodes().get(end).getTimeStamp();
		}

		int i = begin + 1;

		while (i < getSysUseNodes().size() && getSysUseNodes().get(i).getTimeStamp() < endTimeStamp) {

			if (Utility.inPattern(this.getSysUseNodes().get(i), releasePattern)) {
				return i;
			}

			i++;
		}

		return -1;
	}

	public List<EnergyBug> getAllEnergyBugs(List<Integer> bugList, int subType) {

		final List<EnergyBug> bugs = new ArrayList<EnergyBug>();

		for (final Integer i : bugList) {
			final DNode bugNode = getSysUseNodes().get(i);
			addIntoBugTable(bugs, bugNode, subType);
		}

		return bugs;
	}

	public List<EnergyBug> addEnergyBugs(List<EnergyBug> bugs, List<Integer> newBugs, int subType) {
		// immortality bug if subtype = 4

		for (final Integer i : newBugs) {
			final DNode bugNode = getSysUseNodes().get(i);
			addIntoBugTable(bugs, bugNode, subType);
		}

		return bugs;
	}

	private void addIntoBugTable(List<EnergyBug> bugTable, DNode p, int subType) {

		final EnergyBug bug = new EnergyBug(p.getFile(), p.getMethod());
		final DNode caller = p.getParent();

		if (caller.getFile().endsWith("_")) {// hack
			caller.setFile(caller.getFile().substring(0, caller.getFile().lastIndexOf("_")));
			caller.setMethod("_" + caller.getMethod());
		}

		if (caller.getMethod() == null) {
			bug.setCaller(p.getFile() + ";->" + p.getMethod());
		} else {
			bug.setCaller(caller.getFile() + ";->" + caller.getMethod());
		}
		// bug.setCaller(caller.getFile() + ";->" + caller.getMethod());

		final List<String> ms = new ArrayList<String>();

		int count = 5; // 5 next sibling call, if any
		while (p.getNextSibling() != null && count > 0) {
			p = p.getNextSibling();
			ms.add(p.getFile() + ";->" + p.getMethod());
			count--;
		}

		bug.setTimeStamp(p.getTimeStamp());
		bug.setNext5SiblingMethods(ms);
		bug.setSubType(subType);
		bugTable.add(bug);
	}

	private void addIntoBugTable(List<EnergyBug> bugTable, DNode p) {

		final EnergyBug bug = new EnergyBug(p.getFile(), p.getMethod());
		final DNode caller = p.getParent();
		// System.out.println("addIntoBugTable 2 : " + caller);
		if (caller.getMethod() == null) {
			bug.setCaller(p.getFile() + ";->" + p.getMethod());
		} else {
			bug.setCaller(caller.getFile() + ";->" + caller.getMethod());
		}
		final List<String> ms = new ArrayList<String>();

		int count = 5; // 5 next sibling call, if any
		while (p.getNextSibling() != null && count > 0) {
			p = p.getNextSibling();
			ms.add(p.getFile() + ";->" + p.getMethod());
			count--;
		}

		bug.setNext5SiblingMethods(ms);
		bugTable.add(bug);
	}

	public List getActivityAfter(long ts) {

		final List<DNode> after = new ArrayList();
		for (final DNode a : activityNodes) {
			if (a.getTimeStamp() > ts) {

				after.add(a);
			}
		}

		Collections.sort(after, new Comparator<DNode>() {

			@Override
			public int compare(DNode o1, DNode o2) {
				if (o2.getTimeStamp() > o1.getTimeStamp()) {
					return 1;
				} else {
					return 0;
				}
			}
		});

		// for (final DNode b : after) {
		// System.out.println(b.getTimeStamp() + " @ " + b);
		// }

		return after;
	}

	public List getActivityBefore(long ts) {

		final List<DNode> before = new ArrayList();
		for (final DNode a : activityNodes) {
			if (a.getTimeStamp() < ts) {

				before.add(a);
			}
		}

		Collections.sort(before, new Comparator<DNode>() {

			@Override
			public int compare(DNode o1, DNode o2) {
				if (o2.getTimeStamp() > o1.getTimeStamp()) {
					return 1;
				} else {
					return 0;
				}
			}
		});

		// for (final DNode b : before) {
		// System.out.println(b.getTimeStamp() + " @ " + b);
		// }

		return before;
	}

}
